<p align="center">
  <img src="public/images/logo.png" height="200" alt="Logo" />
</p>

## Description

API para sistema de vendas da Pargos.

## Installation

```bash
$ pnpm i
```

## Running the app

```bash
# development
$ pnpm start

# production mode
$ pnpm start:prod
```

## Test

```bash
# unit tests
$ pnpm run test

# e2e tests
$ pnpm run test:e2e

# test coverage
$ pnpm run test:cov
```

## Stay in touch

- Author - [Vaniel Silva](https://vaniel.dev)

## About

## License
