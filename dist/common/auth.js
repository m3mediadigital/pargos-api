"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthGuard = exports.isResolvingGraphQLField = exports.IsPrivate = exports.IsPublic = exports.AuthMiddleware = exports.CurrentSession = exports.CurrentUser = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const session_1 = require("../model/session");
const request_1 = require("./request");
const graphql_1 = require("@nestjs/graphql");
const messages_1 = require("./messages");
exports.CurrentUser = common_1.createParamDecorator((prop, ctx) => {
    const req = request_1.getRequest(ctx);
    return prop ? req.currentUser && req.currentUser[prop] : req.currentUser;
});
exports.CurrentSession = common_1.createParamDecorator((_, ctx) => {
    const req = request_1.getRequest(ctx);
    return req.session;
});
async function AuthMiddleware(req, res, next) {
    try {
        req.headers['authorization'] = req.headers['authorization']?.replace('Bearer ', '');
        if (req.headers['authorization']) {
            req.session = await session_1.Session.createQueryBuilder('session')
                .innerJoinAndSelect('session.user', 'user')
                .where(`session.token = :token AND ((session.updated > NOW() - INTERVAL '6 hours') OR (session.expirable = FALSE))`, {
                token: req.headers['authorization'],
            })
                .getOne();
            if (req.session) {
                await req.session.touch();
                req.currentUser = req.session.user;
            }
        }
    }
    catch (ignore) { }
    next();
}
exports.AuthMiddleware = AuthMiddleware;
function IsPublic({ userRequired = true } = {}) {
    return common_1.applyDecorators(common_1.SetMetadata('userRequired', userRequired), common_1.SetMetadata('public', true));
}
exports.IsPublic = IsPublic;
function IsPrivate() {
    return common_1.applyDecorators(common_1.SetMetadata('userRequired', true), common_1.SetMetadata('public', false));
}
exports.IsPrivate = IsPrivate;
function isResolvingGraphQLField(context) {
    if (context.getType() === 'graphql') {
        const gqlContext = graphql_1.GqlExecutionContext.create(context);
        const info = gqlContext.getInfo();
        const parentType = info.parentType.name;
        return parentType !== 'Query' && parentType !== 'Mutation';
    }
    return false;
}
exports.isResolvingGraphQLField = isResolvingGraphQLField;
let AuthGuard = class AuthGuard {
    constructor(reflector) {
        this.reflector = reflector;
    }
    async canActivate(ctx) {
        const req = request_1.getRequest(ctx);
        const userRequired = !!this.reflector.getAllAndOverride('userRequired', [ctx.getClass(), ctx.getHandler()]);
        if (userRequired && !req.currentUser) {
            const res = request_1.getResponse(ctx);
            res.status(401);
            throw new common_1.UnauthorizedException(messages_1.default.notAuthenticated);
        }
        const isPublic = !!this.reflector.getAllAndOverride('public', [
            ctx.getClass(),
            ctx.getHandler(),
        ]);
        if (isPublic)
            return true;
        const userPermission = req.currentUser.role;
        if (!userPermission)
            return false;
        const superResource = ctx
            .getClass()
            .name.replace(/Resolver$/, '')
            .toLowerCase();
        const resource = [superResource, ctx.getHandler().name]
            .join('.')
            .toLowerCase();
        if (userPermission === 'root')
            return true;
        if (userPermission === superResource)
            return true;
        return userPermission === resource;
    }
};
AuthGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [core_1.Reflector])
], AuthGuard);
exports.AuthGuard = AuthGuard;
//# sourceMappingURL=auth.js.map