"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("./util");
const auth_1 = require("./auth");
const configs = util_1.default.getConfigs();
configs.db = {
    type: 'postgres',
    application_name: configs.name,
    ...configs.db,
    ssl: process.env.NODE_ENV === 'production' ? {
        rejectUnauthorized: false,
    } : null,
    entities: [util_1.Path.join(__dirname, '..', 'model', '*{.ts,.js}')],
    migrations: [util_1.Path.join(__dirname, '..', 'migration', '*{.ts,.js}')],
    migrationsRun: true,
    cli: {
        entitiesDir: util_1.Path.join(process.cwd(), 'src', 'model'),
        subscribersDir: util_1.Path.join(process.cwd(), 'src', 'subscriber'),
        migrationsDir: util_1.Path.join(process.cwd(), 'src', 'migration'),
    },
};
configs.api = {
    autoSchemaFile: 'schema.gql',
    fieldResolverEnhancers: ['guards'],
    ...configs.api,
    context({ req, res, connection }) {
        return {
            req: connection ? connection.context : req,
            res,
        };
    },
    subscriptions: {
        onConnect(connectionParams, _, ctx) {
            ctx.request.headers['authorization'] =
                connectionParams['Authorization'] || connectionParams['authorization'];
            return new Promise((resolve) => {
                auth_1.AuthMiddleware(ctx.request, null, () => {
                    resolve(ctx.request);
                });
            });
        },
    },
};
exports.default = configs;
//# sourceMappingURL=config.js.map