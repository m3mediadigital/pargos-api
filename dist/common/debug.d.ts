import Debug from 'debug';
export default function debug(...modulesNames: string[]): Debug.Debugger;
