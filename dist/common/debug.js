"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const debug_1 = require("debug");
function debug(...modulesNames) {
    return debug_1.default([process.env.npm_package_name, ...modulesNames].join(':'));
}
exports.default = debug;
//# sourceMappingURL=debug.js.map