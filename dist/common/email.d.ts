import { SendMailOptions } from 'nodemailer';
import { render as RenderMail, Options as RenderMailOptions } from 'ejs';
declare const Mail: import("nodemailer").Transporter<import("nodemailer/lib/smtp-transport").SentMessageInfo>;
export { Mail, SendMailOptions, RenderMail, RenderMailOptions };
