"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RenderMail = exports.Mail = void 0;
const nodemailer_1 = require("nodemailer");
const nodemailer_html_to_text_1 = require("nodemailer-html-to-text");
const ejs_1 = require("ejs");
Object.defineProperty(exports, "RenderMail", { enumerable: true, get: function () { return ejs_1.render; } });
const config_1 = require("./config");
const Mail = nodemailer_1.createTransport(config_1.default.email.transport);
exports.Mail = Mail;
Mail.use('compile', nodemailer_html_to_text_1.htmlToText());
//# sourceMappingURL=email.js.map