import { ArgumentsHost } from '@nestjs/common';
import { Session } from '../model/session';
import { User } from '../model/user';
export interface MyRequest extends Request {
    ip: string;
    session: Session;
    currentUser: User;
}
export declare function getRequest(ctx: ArgumentsHost): any;
export declare function getResponse(ctx: ArgumentsHost): any;
