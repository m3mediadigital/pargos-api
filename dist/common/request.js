"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getResponse = exports.getRequest = void 0;
const graphql_1 = require("@nestjs/graphql");
function getRequest(ctx) {
    if (ctx.getType().toString() === 'graphql' || !!ctx.getArgByIndex(3)) {
        const req = graphql_1.GqlArgumentsHost.create(ctx).getContext().req;
        return req.raw || req;
    }
    else {
        return ctx.switchToHttp().getRequest();
    }
}
exports.getRequest = getRequest;
function getResponse(ctx) {
    if (ctx.getType().toString() === 'graphql' || !!ctx.getArgByIndex(3)) {
        return graphql_1.GqlArgumentsHost.create(ctx).getContext().res;
    }
    else {
        return ctx.switchToHttp().getResponse();
    }
}
exports.getResponse = getResponse;
//# sourceMappingURL=request.js.map