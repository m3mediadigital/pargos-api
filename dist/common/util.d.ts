/// <reference types="node" />
import * as URL from 'url';
import * as HTTP from 'http';
import * as Path from 'path';
import * as FS from 'fs';
import * as Crypto from 'crypto';
import * as Bcrypt from 'bcrypt';
import * as UUID from 'uuid';
import * as JS from 'util';
import deepmerge from 'deepmerge';
import * as YAML from 'js-yaml';
export declare class RequestResult extends HTTP.IncomingMessage {
    constructor(res: HTTP.IncomingMessage);
    receiveBody(): Promise<any[]>;
    toText(encoding?: BufferEncoding): Promise<string>;
    toJson(encoding?: BufferEncoding): Promise<any>;
}
export default class Util {
    static get js(): {
        merge: typeof deepmerge;
        mergeObjects(dest: any, src: any, redefine?: boolean): any;
        mixClasses(baseClass: any, ...mixins: any[]): {
            new (...args: any[]): {
                [x: string]: any;
            };
            [x: string]: any;
        };
        promiseReflect(promise: any): any;
        arrayGroupBy(array: any, key: any): any;
        arrayLast(array: any): any;
        eventToPromise(events: any, resolveName: any, rejectName?: string): Promise<unknown>;
        format(format?: any, ...param: any[]): string;
        formatWithOptions(inspectOptions: JS.InspectOptions, format?: any, ...param: any[]): string;
        log(string: string): void;
        inspect: typeof JS.inspect;
        isArray(object: any): object is any[];
        isRegExp(object: any): object is RegExp;
        isDate(object: any): object is Date;
        isError(object: any): object is Error;
        inherits(constructor: any, superConstructor: any): void;
        debuglog(key: string): (msg: string, ...param: any[]) => void;
        isBoolean(object: any): object is boolean;
        isBuffer(object: any): object is Buffer;
        isFunction(object: any): boolean;
        isNull(object: any): object is null;
        isNullOrUndefined(object: any): object is null;
        isNumber(object: any): object is number;
        isObject(object: any): boolean;
        isPrimitive(object: any): boolean;
        isString(object: any): object is string;
        isSymbol(object: any): object is symbol;
        isUndefined(object: any): object is undefined;
        deprecate<T extends Function>(fn: T, message: string, code?: string): T;
        isDeepStrictEqual(val1: any, val2: any): boolean;
        callbackify(fn: () => Promise<void>): (callback: (err: NodeJS.ErrnoException) => void) => void;
        callbackify<TResult>(fn: () => Promise<TResult>): (callback: (err: NodeJS.ErrnoException, result: TResult) => void) => void;
        callbackify<T1>(fn: (arg1: T1) => Promise<void>): (arg1: T1, callback: (err: NodeJS.ErrnoException) => void) => void;
        callbackify<T1_1, TResult_1>(fn: (arg1: T1_1) => Promise<TResult_1>): (arg1: T1_1, callback: (err: NodeJS.ErrnoException, result: TResult_1) => void) => void;
        callbackify<T1_2, T2>(fn: (arg1: T1_2, arg2: T2) => Promise<void>): (arg1: T1_2, arg2: T2, callback: (err: NodeJS.ErrnoException) => void) => void;
        callbackify<T1_3, T2_1, TResult_2>(fn: (arg1: T1_3, arg2: T2_1) => Promise<TResult_2>): (arg1: T1_3, arg2: T2_1, callback: (err: NodeJS.ErrnoException, result: TResult_2) => void) => void;
        callbackify<T1_4, T2_2, T3>(fn: (arg1: T1_4, arg2: T2_2, arg3: T3) => Promise<void>): (arg1: T1_4, arg2: T2_2, arg3: T3, callback: (err: NodeJS.ErrnoException) => void) => void;
        callbackify<T1_5, T2_3, T3_1, TResult_3>(fn: (arg1: T1_5, arg2: T2_3, arg3: T3_1) => Promise<TResult_3>): (arg1: T1_5, arg2: T2_3, arg3: T3_1, callback: (err: NodeJS.ErrnoException, result: TResult_3) => void) => void;
        callbackify<T1_6, T2_4, T3_2, T4>(fn: (arg1: T1_6, arg2: T2_4, arg3: T3_2, arg4: T4) => Promise<void>): (arg1: T1_6, arg2: T2_4, arg3: T3_2, arg4: T4, callback: (err: NodeJS.ErrnoException) => void) => void;
        callbackify<T1_7, T2_5, T3_3, T4_1, TResult_4>(fn: (arg1: T1_7, arg2: T2_5, arg3: T3_3, arg4: T4_1) => Promise<TResult_4>): (arg1: T1_7, arg2: T2_5, arg3: T3_3, arg4: T4_1, callback: (err: NodeJS.ErrnoException, result: TResult_4) => void) => void;
        callbackify<T1_8, T2_6, T3_4, T4_2, T5>(fn: (arg1: T1_8, arg2: T2_6, arg3: T3_4, arg4: T4_2, arg5: T5) => Promise<void>): (arg1: T1_8, arg2: T2_6, arg3: T3_4, arg4: T4_2, arg5: T5, callback: (err: NodeJS.ErrnoException) => void) => void;
        callbackify<T1_9, T2_7, T3_5, T4_3, T5_1, TResult_5>(fn: (arg1: T1_9, arg2: T2_7, arg3: T3_5, arg4: T4_3, arg5: T5_1) => Promise<TResult_5>): (arg1: T1_9, arg2: T2_7, arg3: T3_5, arg4: T4_3, arg5: T5_1, callback: (err: NodeJS.ErrnoException, result: TResult_5) => void) => void;
        callbackify<T1_10, T2_8, T3_6, T4_4, T5_2, T6>(fn: (arg1: T1_10, arg2: T2_8, arg3: T3_6, arg4: T4_4, arg5: T5_2, arg6: T6) => Promise<void>): (arg1: T1_10, arg2: T2_8, arg3: T3_6, arg4: T4_4, arg5: T5_2, arg6: T6, callback: (err: NodeJS.ErrnoException) => void) => void;
        callbackify<T1_11, T2_9, T3_7, T4_5, T5_3, T6_1, TResult_6>(fn: (arg1: T1_11, arg2: T2_9, arg3: T3_7, arg4: T4_5, arg5: T5_3, arg6: T6_1) => Promise<TResult_6>): (arg1: T1_11, arg2: T2_9, arg3: T3_7, arg4: T4_5, arg5: T5_3, arg6: T6_1, callback: (err: NodeJS.ErrnoException, result: TResult_6) => void) => void;
        promisify: typeof JS.promisify;
        types: typeof JS.types;
        TextDecoder: typeof JS.TextDecoder;
        TextEncoder: typeof JS.TextEncoder;
    };
    static get SchemaYAML(): YAML.Schema;
    static getConfigs(): any;
    static getMessages(): any;
}
export { Path, FS, URL, Crypto, Bcrypt, UUID };
