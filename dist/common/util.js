"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UUID = exports.Bcrypt = exports.Crypto = exports.URL = exports.FS = exports.Path = exports.RequestResult = void 0;
const URL = require("url");
exports.URL = URL;
const HTTP = require("http");
const Path = require("path");
exports.Path = Path;
const FS = require("fs");
exports.FS = FS;
const Crypto = require("crypto");
exports.Crypto = Crypto;
const Bcrypt = require("bcrypt");
exports.Bcrypt = Bcrypt;
const UUID = require("uuid");
exports.UUID = UUID;
const JS = require("util");
const deepmerge_1 = require("deepmerge");
const YAML = require("js-yaml");
class RequestResult extends HTTP.IncomingMessage {
    constructor(res) {
        super(res.socket);
    }
    receiveBody() {
        return new Promise((resolve, reject) => {
            let rawData = [];
            this.on('data', (chunk) => {
                rawData.push(chunk);
            });
            this.on('end', () => {
                resolve(rawData);
            });
            this.on('error', (err) => reject(err));
        });
    }
    async toText(encoding = 'utf8') {
        this.setEncoding(encoding);
        const result = await this.receiveBody();
        return result.join('');
    }
    async toJson(encoding = 'utf8') {
        this.setEncoding(encoding);
        const result = await this.receiveBody();
        return JSON.parse(result.join(''));
    }
}
exports.RequestResult = RequestResult;
class Util {
    static get js() {
        return {
            ...JS,
            merge: deepmerge_1.default,
            mergeObjects(dest, src, redefine = true) {
                if (!dest) {
                    throw new TypeError('argument dest is required');
                }
                if (!src) {
                    throw new TypeError('argument src is required');
                }
                const hasOwnProperty = Object.prototype.hasOwnProperty;
                Object.getOwnPropertyNames(src).forEach(function forEachOwnPropertyName(name) {
                    if (!redefine && hasOwnProperty.call(dest, name)) {
                        return;
                    }
                    const descriptor = Object.getOwnPropertyDescriptor(src, name);
                    Object.defineProperty(dest, name, descriptor);
                });
                return dest;
            },
            mixClasses(baseClass, ...mixins) {
                const base = class _Combined extends baseClass {
                    constructor(...args) {
                        super(...args);
                        mixins.forEach((mixin) => {
                            mixin.prototype.initializer.call(this);
                        });
                    }
                };
                const copyProps = (target, source) => {
                    Object.getOwnPropertyNames(source)
                        .forEach((prop) => {
                        if (prop.match(/^(?:constructor|prototype|arguments|caller|name|bind|call|apply|toString|length)$/))
                            return;
                        Object.defineProperty(target, prop, Object.getOwnPropertyDescriptor(source, prop));
                    });
                };
                mixins.forEach((mixin) => {
                    copyProps(base.prototype, mixin.prototype);
                    copyProps(base, mixin);
                });
                return base;
            },
            promiseReflect(promise) {
                return promise.then((result) => ({ result, status: 'fulfilled' }), (error) => ({ error, status: 'rejected' }));
            },
            arrayGroupBy(array, key) {
                return array.reduce((m, v) => {
                    (m[v[key]] = m[v[key]] || []).push(v);
                    return m;
                }, {});
            },
            arrayLast(array) {
                return array[array.length - 1];
            },
            eventToPromise(events, resolveName, rejectName = 'error') {
                return new Promise((resolve, reject) => {
                    events.on(resolveName, resolve);
                    events.on(rejectName, reject);
                });
            },
        };
    }
    static get SchemaYAML() {
        const EnvYamlType = new YAML.Type('!env', {
            kind: 'scalar',
            construct(data) {
                const result = data !== null ? process.env[data] : '';
                return result === 'true' ? true : result;
            },
        });
        const EvalYamlType = new YAML.Type('!eval', {
            kind: 'scalar',
            construct(data) {
                return data !== null ? eval(data) : '';
            },
        });
        const OrYamlType = new YAML.Type('!or', {
            kind: 'sequence',
            construct(data) {
                if (data === null)
                    return null;
                return data.reduce((m, v) => m || v, null);
            },
        });
        return new YAML.Schema([EnvYamlType, EvalYamlType, OrYamlType]);
    }
    static getConfigs() {
        const schema = Util.SchemaYAML;
        const pathConfigDefault = Path.resolve(process.cwd(), 'config.default.yaml');
        let configs = YAML.load(FS.readFileSync(pathConfigDefault, 'utf8'), {
            schema,
        });
        if (process.env.NODE_ENV === 'production') {
            const pathConfigProd = Path.resolve(process.cwd(), 'config.prod.yaml');
            if (FS.existsSync(pathConfigProd)) {
                const configsProd = YAML.load(FS.readFileSync(pathConfigProd, 'utf8'), {
                    schema,
                });
                if (configsProd)
                    configs = deepmerge_1.default(configs, configsProd);
            }
        }
        const pathConfigLocal = Path.resolve(process.cwd(), 'config.local.yaml');
        if (FS.existsSync(pathConfigLocal)) {
            const configsLocal = YAML.load(FS.readFileSync(pathConfigLocal, 'utf8'), {
                schema,
            });
            if (configsLocal)
                configs = deepmerge_1.default(configs, configsLocal);
        }
        return configs;
    }
    static getMessages() {
        const schema = Util.SchemaYAML;
        const pathFile = Path.resolve(process.cwd(), 'src', 'locales', 'default.yaml');
        return YAML.load(FS.readFileSync(pathFile, 'utf8'), { schema });
    }
}
exports.default = Util;
//# sourceMappingURL=util.js.map