"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
require("reflect-metadata");
const module_1 = require("./module");
const auth_1 = require("./common/auth");
const config_1 = require("./common/config");
async function bootstrap() {
    const app = await core_1.NestFactory.create(module_1.AppModule);
    app.enableCors(config_1.default.cors);
    app.use(auth_1.AuthMiddleware);
    await app.listen(config_1.default.port, config_1.default.host);
}
bootstrap();
//# sourceMappingURL=main.js.map