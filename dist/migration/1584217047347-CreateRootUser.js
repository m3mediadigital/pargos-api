"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateRootUser1584217047347 = void 0;
class CreateRootUser1584217047347 {
    async up(queryRunner) {
        return queryRunner.query(`INSERT INTO "user"
			("email", "password", "role") VALUES
			('user@email.com', '$2b$10$2Vlm.WzHZ32PRO3V.VRHcuw6BMkRlnBIh0iBZ9j/QAGTMq5ed6tWS','root')
		;`);
    }
    async down(queryRunner) {
        return queryRunner.query(`
			DELETE FROM "user" CASCATE
			WHERE "email" = 'user@email.com'
		;`);
    }
}
exports.CreateRootUser1584217047347 = CreateRootUser1584217047347;
//# sourceMappingURL=1584217047347-CreateRootUser.js.map