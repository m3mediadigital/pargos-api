export declare class Address {
    street: string;
    cep: string;
    district: string;
    city: string;
    uf: string;
}
