import { BaseEntity as OrmEntity, ObjectType as TObjectType, ObjectLiteral, OrderByCondition } from 'typeorm';
import { GraphQLScalarType } from 'graphql';
import { Pagination, PageInfo } from '../type/pagination';
export declare class BaseEntity extends OrmEntity {
    static findOneByArgs<T extends BaseEntity>(args: GetOneArgs): Promise<T>;
    static findPage<T extends BaseEntity>(this: TObjectType<T>, args: GetManyArgs): any;
    static findPage<T extends BaseEntity>(this: TObjectType<T>, options?: FindPageOptions<T>, args?: GetManyArgs): any;
}
export declare abstract class Node extends BaseEntity {
    id: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
}
export declare const Json: GraphQLScalarType;
export declare abstract class Payload {
    result: Node[];
}
export declare abstract class Connection {
    nodes: Node[];
    pageInfo: PageInfo;
}
export declare class GetOneArgs {
    id: string;
}
export declare class GetManyArgs {
    q?: string;
    pagination: Pagination;
}
export interface FindPageOptions<Entity = any> {
    where?: string | ((qb: this) => string) | ObjectLiteral | ObjectLiteral[];
    parameters?: ObjectLiteral;
    order?: OrderByCondition;
}
