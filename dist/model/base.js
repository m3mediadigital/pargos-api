"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetManyArgs = exports.GetOneArgs = exports.Connection = exports.Payload = exports.Json = exports.Node = exports.BaseEntity = void 0;
const typeorm_1 = require("typeorm");
const graphql_1 = require("@nestjs/graphql");
const graphql_2 = require("graphql");
const pagination_1 = require("../type/pagination");
class BaseEntity extends typeorm_1.BaseEntity {
    static findOneByArgs(args) {
        return this.findOne(args.id);
    }
    static async findPage(...params) {
        let options = {};
        let args;
        if (params.length === 1) {
            [args] = params;
        }
        else {
            [options, args] = params;
        }
        const current = (args.pagination.after
            ? parseInt(args.pagination.after)
            : args.pagination.page) || 0;
        const query = typeorm_1.getConnection().createQueryBuilder(this, this.name);
        if (!args.pagination.disabled) {
            query.take(args.pagination.limit);
        }
        query.skip(current * args.pagination.limit);
        if (options.where) {
            query.where(options.where);
        }
        if (options.parameters) {
            query.setParameters(options.parameters);
        }
        if (options.order) {
            query.orderBy(options.order);
        }
        if (args.q && Reflect.hasMetadata('queryKeys', this)) {
            const keys = Reflect.getMetadata('queryKeys', this);
            const keysParsed = keys
                .map((key) => {
                return `COALESCE("${this.name}"."${key}"::TEXT)`;
            })
                .filter((key) => !!key);
            if (keysParsed.length > 0) {
                query.andWhere(`TO_TSVECTOR('portuguese', ${keysParsed.join(` || ' ' || `)}) @@ WEBSEARCH_TO_TSQUERY('portuguese', :q)`);
                query.orderBy(`TS_RANK_CD(TO_TSVECTOR('portuguese', ${keysParsed.join(` || ' ' || `)}), WEBSEARCH_TO_TSQUERY('portuguese', :q))`);
                query.setParameter('q', args.q);
            }
        }
        const [nodes, count] = await query.getManyAndCount();
        return {
            nodes,
            pageInfo: {
                count,
                pages: Math.ceil(count / args.pagination.limit),
                current,
            },
        };
    }
}
exports.BaseEntity = BaseEntity;
let Node = class Node extends BaseEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ type: 'bigint' }),
    graphql_1.Field(() => graphql_1.ID),
    __metadata("design:type", String)
], Node.prototype, "id", void 0);
__decorate([
    typeorm_1.CreateDateColumn(),
    graphql_1.Field(),
    __metadata("design:type", Date)
], Node.prototype, "createdAt", void 0);
__decorate([
    typeorm_1.UpdateDateColumn(),
    graphql_1.Field(),
    __metadata("design:type", Date)
], Node.prototype, "updatedAt", void 0);
__decorate([
    typeorm_1.DeleteDateColumn(),
    __metadata("design:type", Date)
], Node.prototype, "deletedAt", void 0);
Node = __decorate([
    graphql_1.InterfaceType()
], Node);
exports.Node = Node;
exports.Json = new graphql_2.GraphQLScalarType({
    name: 'JSON',
    serialize(value) {
        return value;
    },
});
let Payload = class Payload {
};
__decorate([
    graphql_1.Field(() => [Node]),
    __metadata("design:type", Array)
], Payload.prototype, "result", void 0);
Payload = __decorate([
    graphql_1.InterfaceType()
], Payload);
exports.Payload = Payload;
let Connection = class Connection {
};
__decorate([
    graphql_1.Field(() => [Node]),
    __metadata("design:type", Array)
], Connection.prototype, "nodes", void 0);
__decorate([
    graphql_1.Field(() => pagination_1.PageInfo),
    __metadata("design:type", pagination_1.PageInfo)
], Connection.prototype, "pageInfo", void 0);
Connection = __decorate([
    graphql_1.InterfaceType()
], Connection);
exports.Connection = Connection;
let GetOneArgs = class GetOneArgs {
};
__decorate([
    graphql_1.Field(() => graphql_1.ID),
    __metadata("design:type", String)
], GetOneArgs.prototype, "id", void 0);
GetOneArgs = __decorate([
    graphql_1.ArgsType()
], GetOneArgs);
exports.GetOneArgs = GetOneArgs;
let GetManyArgs = class GetManyArgs {
};
__decorate([
    graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], GetManyArgs.prototype, "q", void 0);
__decorate([
    graphql_1.Field({ nullable: true, defaultValue: { limit: 20 } }),
    __metadata("design:type", pagination_1.Pagination)
], GetManyArgs.prototype, "pagination", void 0);
GetManyArgs = __decorate([
    graphql_1.ArgsType()
], GetManyArgs);
exports.GetManyArgs = GetManyArgs;
//# sourceMappingURL=base.js.map