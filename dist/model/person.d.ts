import { Address } from './address';
export declare class Person {
    name: string;
    sex: string;
    rg: string;
    rgInfo: string;
    cpf: string;
    nationality: string;
    birthday: Date;
    maritalStatus: string;
    profission: string;
    address: Address;
    phone: string;
}
