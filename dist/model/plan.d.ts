import { Node } from './base';
import { Sale } from './sale';
export declare class Plan extends Node {
    name: string;
    value: number;
    comission: number;
    info?: string;
    sales: Sale[];
}
