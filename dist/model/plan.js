"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Plan = void 0;
const typeorm_1 = require("typeorm");
const graphql_1 = require("@nestjs/graphql");
const common_1 = require("@nestjs/common");
const base_1 = require("./base");
const sale_1 = require("./sale");
let Plan = class Plan extends base_1.Node {
};
__decorate([
    typeorm_1.Column('text'),
    graphql_1.Field(),
    __metadata("design:type", String)
], Plan.prototype, "name", void 0);
__decorate([
    typeorm_1.Column('numeric'),
    graphql_1.Field(),
    __metadata("design:type", Number)
], Plan.prototype, "value", void 0);
__decorate([
    typeorm_1.Column('numeric'),
    graphql_1.Field(),
    __metadata("design:type", Number)
], Plan.prototype, "comission", void 0);
__decorate([
    typeorm_1.Column('text', { nullable: true }),
    graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], Plan.prototype, "info", void 0);
__decorate([
    typeorm_1.OneToMany(() => sale_1.Sale, (sale) => sale.plan),
    __metadata("design:type", Array)
], Plan.prototype, "sales", void 0);
Plan = __decorate([
    typeorm_1.Entity({
        orderBy: { id: 'ASC' },
    }),
    graphql_1.ObjectType({ implements: base_1.Node }),
    common_1.SetMetadata('queryKeys', ['name'])
], Plan);
exports.Plan = Plan;
//# sourceMappingURL=plan.js.map