import { Node } from './base';
import { User } from './user';
import { Address } from './address';
import { Plan } from './plan';
import { Person } from './person';
export declare class Sale extends Node {
    user: User;
    userId: string;
    plan: Plan;
    planId: string;
    person: Person;
    email: string;
    address2: Address;
    phone2: string;
    dependents: string[];
    confirmed: Date;
}
