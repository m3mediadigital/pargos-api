"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Sale = void 0;
const typeorm_1 = require("typeorm");
const graphql_1 = require("@nestjs/graphql");
const common_1 = require("@nestjs/common");
const base_1 = require("./base");
const user_1 = require("./user");
const address_1 = require("./address");
const plan_1 = require("./plan");
const person_1 = require("./person");
let Sale = class Sale extends base_1.Node {
};
__decorate([
    typeorm_1.ManyToOne(() => user_1.User, (user) => user.sales),
    graphql_1.Field(() => user_1.User),
    __metadata("design:type", user_1.User)
], Sale.prototype, "user", void 0);
__decorate([
    typeorm_1.RelationId((sale) => sale.user),
    graphql_1.Field(() => graphql_1.ID),
    __metadata("design:type", String)
], Sale.prototype, "userId", void 0);
__decorate([
    typeorm_1.ManyToOne(() => plan_1.Plan, (plan) => plan.sales, { eager: true }),
    graphql_1.Field(() => plan_1.Plan),
    __metadata("design:type", plan_1.Plan)
], Sale.prototype, "plan", void 0);
__decorate([
    typeorm_1.RelationId((sale) => sale.plan),
    graphql_1.Field(() => graphql_1.ID),
    __metadata("design:type", String)
], Sale.prototype, "planId", void 0);
__decorate([
    typeorm_1.Column(() => person_1.Person),
    graphql_1.Field(() => person_1.Person),
    __metadata("design:type", person_1.Person)
], Sale.prototype, "person", void 0);
__decorate([
    typeorm_1.Column('text', { nullable: true }),
    graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], Sale.prototype, "email", void 0);
__decorate([
    typeorm_1.Column(() => address_1.Address),
    graphql_1.Field(() => address_1.Address),
    __metadata("design:type", address_1.Address)
], Sale.prototype, "address2", void 0);
__decorate([
    typeorm_1.Column('text', { nullable: true }),
    graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], Sale.prototype, "phone2", void 0);
__decorate([
    typeorm_1.Column('simple-array', { nullable: true }),
    graphql_1.Field(() => [String], { nullable: true }),
    __metadata("design:type", Array)
], Sale.prototype, "dependents", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    graphql_1.Field({ nullable: true }),
    __metadata("design:type", Date)
], Sale.prototype, "confirmed", void 0);
Sale = __decorate([
    typeorm_1.Entity({
        orderBy: { id: 'DESC' },
    }),
    graphql_1.ObjectType({ implements: base_1.Node }),
    common_1.SetMetadata('queryKeys', ['number'])
], Sale);
exports.Sale = Sale;
//# sourceMappingURL=sale.js.map