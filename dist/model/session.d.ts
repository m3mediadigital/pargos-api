import { BaseEntity } from 'typeorm';
import { User } from './user';
export declare class Session extends BaseEntity {
    token: string;
    ip: string;
    user: User;
    created: Date;
    updated: Date;
    expirable: boolean;
    touch(): Promise<import("typeorm").UpdateResult>;
    static ofUser(user: User): Promise<Session[]>;
    static toUser(user: User, ip: string): Promise<Session>;
}
