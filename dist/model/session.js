"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var Session_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.Session = void 0;
const typeorm_1 = require("typeorm");
const graphql_1 = require("@nestjs/graphql");
const user_1 = require("./user");
let Session = Session_1 = class Session extends typeorm_1.BaseEntity {
    touch() {
        return Session_1.update(this.token, {});
    }
    static async ofUser(user) {
        return user.sessions || Session_1.find({ user });
    }
    static async toUser(user, ip) {
        return Session_1.create({ user, ip });
    }
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Session.prototype, "token", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Session.prototype, "ip", void 0);
__decorate([
    typeorm_1.ManyToOne(() => user_1.User, (user) => user.sessions),
    __metadata("design:type", user_1.User)
], Session.prototype, "user", void 0);
__decorate([
    typeorm_1.CreateDateColumn(),
    graphql_1.Field(),
    __metadata("design:type", Date)
], Session.prototype, "created", void 0);
__decorate([
    typeorm_1.UpdateDateColumn(),
    graphql_1.Field(),
    __metadata("design:type", Date)
], Session.prototype, "updated", void 0);
__decorate([
    typeorm_1.Column({ nullable: false, default: false }),
    graphql_1.Field({ nullable: false, defaultValue: false }),
    __metadata("design:type", Boolean)
], Session.prototype, "expirable", void 0);
Session = Session_1 = __decorate([
    typeorm_1.Entity({
        orderBy: { updated: 'DESC' },
    }),
    typeorm_1.Index(['token', 'updated']),
    graphql_1.ObjectType()
], Session);
exports.Session = Session;
//# sourceMappingURL=session.js.map