export declare class System {
    name: string;
    description: string;
    currentTime: Date;
}
