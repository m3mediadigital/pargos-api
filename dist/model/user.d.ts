import { Node } from './base';
import { Session } from './session';
import { Person } from './person';
import { Sale } from './sale';
export declare class User extends Node {
    email: string;
    password: string;
    role: string;
    person: Person;
    sessions: Session[];
    sales: Sale[];
    static findToLogin(value: any): Promise<User>;
}
