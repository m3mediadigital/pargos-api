"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var User_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const typeorm_1 = require("typeorm");
const graphql_1 = require("@nestjs/graphql");
const common_1 = require("@nestjs/common");
const base_1 = require("./base");
const session_1 = require("./session");
const person_1 = require("./person");
const sale_1 = require("./sale");
let User = User_1 = class User extends base_1.Node {
    static findToLogin(value) {
        return User_1.createQueryBuilder('user')
            .addSelect('user.password')
            .where({ email: value })
            .getOne();
    }
};
__decorate([
    typeorm_1.Column('text', { unique: true }),
    graphql_1.Field(),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    typeorm_1.Column('text', { select: false }),
    __metadata("design:type", String)
], User.prototype, "password", void 0);
__decorate([
    typeorm_1.Column('text'),
    graphql_1.Field(),
    __metadata("design:type", String)
], User.prototype, "role", void 0);
__decorate([
    typeorm_1.Column(() => person_1.Person),
    graphql_1.Field({ nullable: true }),
    __metadata("design:type", person_1.Person)
], User.prototype, "person", void 0);
__decorate([
    typeorm_1.OneToMany(() => session_1.Session, (session) => session.user),
    graphql_1.Field(() => [session_1.Session]),
    __metadata("design:type", Array)
], User.prototype, "sessions", void 0);
__decorate([
    typeorm_1.OneToMany(() => sale_1.Sale, (sale) => sale.user),
    graphql_1.Field(() => [sale_1.Sale]),
    __metadata("design:type", Array)
], User.prototype, "sales", void 0);
User = User_1 = __decorate([
    typeorm_1.Entity({
        orderBy: { id: 'ASC' },
    }),
    graphql_1.ObjectType({ implements: base_1.Node }),
    common_1.SetMetadata('queryKeys', ['email', 'name'])
], User);
exports.User = User;
//# sourceMappingURL=user.js.map