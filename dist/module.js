"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const core_1 = require("@nestjs/core");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const graphql_1 = require("@nestjs/graphql");
const auth_1 = require("./common/auth");
const config_1 = require("./common/config");
const system_1 = require("./resolver/system");
const user_1 = require("./resolver/user");
const plan_1 = require("./resolver/plan");
const sale_1 = require("./resolver/sale");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        providers: [
            { provide: core_1.APP_GUARD, useClass: auth_1.AuthGuard },
        ],
        imports: [
            typeorm_1.TypeOrmModule.forRoot(config_1.default.db),
            graphql_1.GraphQLModule.forRoot(config_1.default.api),
            system_1.SystemResolver,
            user_1.UserResolver,
            plan_1.PlanResolver,
            sale_1.SaleResolver,
        ],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=module.js.map