import { GetOneArgs, GetManyArgs } from '../model/base';
import { Plan } from '../model/plan';
import { PlanCreateInput, PlanUpdateInput } from '../type/plan.dto';
export declare class PlanResolver {
    queryPlan(args: GetOneArgs): Promise<Plan>;
    queryPlans(args: GetManyArgs): any;
    mutationPlanCreate(input: PlanCreateInput): Promise<Plan>;
    mutationPlanUpdate(args: GetOneArgs, input: PlanUpdateInput): Promise<Plan>;
}
