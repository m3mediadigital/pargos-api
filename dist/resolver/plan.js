"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlanResolver = void 0;
const graphql_1 = require("@nestjs/graphql");
const auth_1 = require("../common/auth");
const base_1 = require("../model/base");
const plan_1 = require("../model/plan");
const plan_dto_1 = require("../type/plan.dto");
let PlanResolver = class PlanResolver {
    queryPlan(args) {
        return plan_1.Plan.findOne(args);
    }
    queryPlans(args) {
        return plan_1.Plan.findPage(args);
    }
    async mutationPlanCreate(input) {
        return plan_1.Plan.create(input).save();
    }
    async mutationPlanUpdate(args, input) {
        const plan = await plan_1.Plan.findOne({ id: args.id });
        if (!plan)
            throw new Error('Registro não encontrado!');
        await plan_1.Plan.update({ id: plan.id }, input);
        await plan.reload();
        return plan;
    }
};
__decorate([
    graphql_1.Query(() => plan_1.Plan, { name: 'plan' }),
    auth_1.IsPublic({ userRequired: false }),
    __param(0, graphql_1.Args()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [base_1.GetOneArgs]),
    __metadata("design:returntype", void 0)
], PlanResolver.prototype, "queryPlan", null);
__decorate([
    graphql_1.Query(() => plan_dto_1.PlanConnection, { name: 'plans' }),
    auth_1.IsPublic({ userRequired: false }),
    __param(0, graphql_1.Args()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [base_1.GetManyArgs]),
    __metadata("design:returntype", void 0)
], PlanResolver.prototype, "queryPlans", null);
__decorate([
    graphql_1.Mutation(() => plan_1.Plan, { name: 'planCreate' }),
    __param(0, graphql_1.Args('input')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [plan_dto_1.PlanCreateInput]),
    __metadata("design:returntype", Promise)
], PlanResolver.prototype, "mutationPlanCreate", null);
__decorate([
    graphql_1.Mutation(() => plan_1.Plan, { name: 'planUpdate' }),
    __param(0, graphql_1.Args()),
    __param(1, graphql_1.Args('input')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [base_1.GetOneArgs,
        plan_dto_1.PlanUpdateInput]),
    __metadata("design:returntype", Promise)
], PlanResolver.prototype, "mutationPlanUpdate", null);
PlanResolver = __decorate([
    graphql_1.Resolver(() => plan_1.Plan)
], PlanResolver);
exports.PlanResolver = PlanResolver;
//# sourceMappingURL=plan.js.map