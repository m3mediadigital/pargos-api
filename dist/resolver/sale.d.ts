import { User } from '../model/user';
import { Sale } from '../model/sale';
import { GetOneArgs, GetManyArgs } from '../model/base';
import { SaleCreateInput, SaleUpdateInput } from '../type/sale.dto';
export declare class SaleResolver {
    user(sale: Sale): Promise<any>;
    plan(sale: Sale): Promise<any>;
    isConfirmed(sale: Sale): boolean;
    querySale(args: GetOneArgs, admin: boolean, user: User): Promise<Sale>;
    querySales(args: GetManyArgs, admin: boolean, saler: string, user: User): any;
    queryComissions(args: GetManyArgs, user: User): Promise<{
        nodes: any;
        pageInfo: any;
    }>;
    queryTotal(user: User): Promise<number>;
    queryTotalComplete(user: User): Promise<number>;
    queryValueIncoming(user: User): Promise<any>;
    queryValueTotal(user: User): Promise<any>;
    queryGraph(user: User): Promise<{
        title: string;
        minX: number;
        maxX: any;
        minY: number;
        maxY: number;
        data: any;
    }>;
    mutationSaleCreate(input: SaleCreateInput, user: User): Promise<Sale>;
    mutationSaleUpdate(args: GetOneArgs, input: SaleUpdateInput, admin: boolean, user: User): Promise<Sale>;
}
