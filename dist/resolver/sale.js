"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SaleResolver = void 0;
const typeorm_1 = require("typeorm");
const graphql_1 = require("@nestjs/graphql");
const auth_1 = require("../common/auth");
const user_1 = require("../model/user");
const sale_1 = require("../model/sale");
const base_1 = require("../model/base");
const sale_dto_1 = require("../type/sale.dto");
const plan_1 = require("../model/plan");
const monthNames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
let SaleResolver = class SaleResolver {
    user(sale) {
        return typeorm_1.getConnection()
            .createQueryBuilder()
            .relation(sale_1.Sale, 'user')
            .of(sale)
            .loadOne();
    }
    plan(sale) {
        return typeorm_1.getConnection()
            .createQueryBuilder()
            .relation(sale_1.Sale, 'plan')
            .of(sale)
            .loadOne();
    }
    isConfirmed(sale) {
        return sale.confirmed != null;
    }
    querySale(args, admin, user) {
        if (admin && user.role === 'root') {
            return sale_1.Sale.findOne({ id: args.id });
        }
        else {
            return sale_1.Sale.findOne({ id: args.id, user });
        }
    }
    querySales(args, admin, saler, user) {
        if (admin && user.role === 'root') {
            if (saler) {
                return sale_1.Sale.findPage({ where: "Sale.userId = :saler", parameters: { saler } }, args);
            }
            else {
                return sale_1.Sale.findPage(args);
            }
        }
        else {
            return sale_1.Sale.findPage({ where: { user } }, args);
        }
    }
    async queryComissions(args, user) {
        const [sales, plans] = await Promise.all([
            sale_1.Sale.findPage({
                where: {
                    user,
                    confirmed: typeorm_1.Not(typeorm_1.IsNull()),
                },
            }, args),
            plan_1.Plan.find().then(plans => Object.fromEntries(plans.map(plan => [plan.id, plan]))),
        ]);
        return {
            nodes: sales.nodes.map(sale => {
                const plan = plans[sale.planId];
                return {
                    sale,
                    comission: plan.value * plan.comission,
                    date: sale.confirmed,
                };
            }),
            pageInfo: sales.pageInfo,
        };
    }
    queryTotal(user) {
        return sale_1.Sale.count({ user });
    }
    queryTotalComplete(user) {
        return sale_1.Sale.count({ user, confirmed: typeorm_1.Not(typeorm_1.IsNull()) });
    }
    queryValueIncoming(user) {
        return typeorm_1.getConnection().query(`
			SELECT SUM(p.value * p.comission) AS total
			FROM "sale" s
			INNER JOIN "plan" p ON p.id = s."planId"
			WHERE s."userId" = $1 AND s.confirmed IS NOT NULL AND DATE_TRUNC('month', s."createdAt") = DATE_TRUNC('month', CURRENT_DATE);
		`, [user.id]).then(([{ total }]) => total);
    }
    queryValueTotal(user) {
        return typeorm_1.getConnection().query(`
			SELECT SUM(p.value * p.comission) AS total
			FROM "sale" s
			INNER JOIN "plan" p ON p.id = s."planId"
			WHERE s."userId" = $1 AND s.confirmed IS NOT NULL;
		`, [user.id]).then(([{ total }]) => total);
    }
    async queryGraph(user) {
        const data = await typeorm_1.getConnection().query(`
			SELECT d::DATE AS day, COALESCE(SUM(p.value * p.comission), 0) AS total, ARRAY_AGG(s.id) AS sales
			FROM generate_series(DATE_TRUNC('month', CURRENT_DATE)::DATE, (DATE_TRUNC('month', CURRENT_DATE) + INTERVAL '1 month' - INTERVAL '1 day')::DATE, '1 day'::INTERVAL) d
			LEFT JOIN "sale" s ON DATE_TRUNC('day', s."createdAt") = d AND s."userId" = $1 AND s.confirmed IS NOT NULL
			LEFT JOIN "plan" p ON p.id = s."planId"
			GROUP BY 1
			ORDER BY 1;
		`, [user.id]);
        const max = data.reduce((prev, current) => {
            return current.total > prev ? current.total : prev;
        }, 0);
        return {
            title: monthNames[(new Date()).getMonth()].toUpperCase(),
            minX: 0,
            maxX: data.length,
            minY: 0,
            maxY: max * 1.20,
            data,
        };
    }
    async mutationSaleCreate(input, user) {
        const plan = await plan_1.Plan.findOne(input.planId);
        if (!plan)
            throw new Error('Plano não encontrado!');
        return sale_1.Sale.create({
            ...input,
            plan,
            user: user ?? { id: "1" },
        }).save();
    }
    async mutationSaleUpdate(args, input, admin, user) {
        const sale = (admin && user.role === 'root') ?
            await sale_1.Sale.findOne({ id: args.id }) :
            await sale_1.Sale.findOne({ id: args.id, user });
        if (!sale)
            throw new Error('Registro não encontrado!');
        let data = {
            ...input,
            confirmed: undefined,
            isConfirmed: undefined,
        };
        if (user.role === 'root' && input.isConfirmed !== null) {
            if (input.isConfirmed && sale.confirmed === null) {
                data.confirmed = new Date();
            }
            else if (!input.isConfirmed && sale.confirmed !== null) {
                data.confirmed = null;
            }
        }
        await sale_1.Sale.update({ id: sale.id }, Object.fromEntries(Object.entries(data).filter(([key, value]) => value !== undefined && sale[key] != value)));
        await sale.reload();
        return sale;
    }
};
__decorate([
    graphql_1.ResolveField(),
    __param(0, graphql_1.Root()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [sale_1.Sale]),
    __metadata("design:returntype", void 0)
], SaleResolver.prototype, "user", null);
__decorate([
    graphql_1.ResolveField(),
    __param(0, graphql_1.Root()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [sale_1.Sale]),
    __metadata("design:returntype", void 0)
], SaleResolver.prototype, "plan", null);
__decorate([
    graphql_1.ResolveField(),
    __param(0, graphql_1.Root()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [sale_1.Sale]),
    __metadata("design:returntype", Boolean)
], SaleResolver.prototype, "isConfirmed", null);
__decorate([
    graphql_1.Query(() => sale_1.Sale, { name: 'sale' }),
    auth_1.IsPublic(),
    __param(0, graphql_1.Args()),
    __param(1, graphql_1.Args({ name: 'admin', nullable: true, defaultValue: false })),
    __param(2, auth_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [base_1.GetOneArgs, Boolean, user_1.User]),
    __metadata("design:returntype", void 0)
], SaleResolver.prototype, "querySale", null);
__decorate([
    graphql_1.Query(() => sale_dto_1.SaleConnection, { name: 'sales' }),
    auth_1.IsPublic(),
    __param(0, graphql_1.Args()),
    __param(1, graphql_1.Args({ name: 'admin', nullable: true, defaultValue: false })),
    __param(2, graphql_1.Args({ name: 'userId', type: () => graphql_1.ID, nullable: true })),
    __param(3, auth_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [base_1.GetManyArgs, Boolean, String, user_1.User]),
    __metadata("design:returntype", void 0)
], SaleResolver.prototype, "querySales", null);
__decorate([
    graphql_1.Query(() => sale_dto_1.SaleComissionConnection, { name: 'saleComissions' }),
    auth_1.IsPublic(),
    __param(0, graphql_1.Args()),
    __param(1, auth_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [base_1.GetManyArgs,
        user_1.User]),
    __metadata("design:returntype", Promise)
], SaleResolver.prototype, "queryComissions", null);
__decorate([
    graphql_1.Query(() => graphql_1.Int, { name: 'saleTotal' }),
    auth_1.IsPublic(),
    __param(0, auth_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_1.User]),
    __metadata("design:returntype", void 0)
], SaleResolver.prototype, "queryTotal", null);
__decorate([
    graphql_1.Query(() => graphql_1.Int, { name: 'saleTotalComplete' }),
    auth_1.IsPublic(),
    __param(0, auth_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_1.User]),
    __metadata("design:returntype", void 0)
], SaleResolver.prototype, "queryTotalComplete", null);
__decorate([
    graphql_1.Query(() => graphql_1.Float, { name: 'saleValueIncoming' }),
    auth_1.IsPublic(),
    __param(0, auth_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_1.User]),
    __metadata("design:returntype", void 0)
], SaleResolver.prototype, "queryValueIncoming", null);
__decorate([
    graphql_1.Query(() => graphql_1.Float, { name: 'saleValueTotal' }),
    auth_1.IsPublic(),
    __param(0, auth_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_1.User]),
    __metadata("design:returntype", void 0)
], SaleResolver.prototype, "queryValueTotal", null);
__decorate([
    graphql_1.Query(() => sale_dto_1.SaleComissionGraph, { name: 'saleGraph', nullable: true }),
    auth_1.IsPublic(),
    __param(0, auth_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_1.User]),
    __metadata("design:returntype", Promise)
], SaleResolver.prototype, "queryGraph", null);
__decorate([
    graphql_1.Mutation(() => sale_1.Sale, { name: 'saleCreate' }),
    auth_1.IsPublic({ userRequired: false }),
    __param(0, graphql_1.Args('input')),
    __param(1, auth_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [sale_dto_1.SaleCreateInput,
        user_1.User]),
    __metadata("design:returntype", Promise)
], SaleResolver.prototype, "mutationSaleCreate", null);
__decorate([
    graphql_1.Mutation(() => sale_1.Sale, { name: 'saleUpdate' }),
    auth_1.IsPublic(),
    __param(0, graphql_1.Args()),
    __param(1, graphql_1.Args('input')),
    __param(2, graphql_1.Args({ name: 'admin', nullable: true, defaultValue: false })),
    __param(3, auth_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [base_1.GetOneArgs,
        sale_dto_1.SaleUpdateInput, Boolean, user_1.User]),
    __metadata("design:returntype", Promise)
], SaleResolver.prototype, "mutationSaleUpdate", null);
SaleResolver = __decorate([
    graphql_1.Resolver(() => sale_1.Sale)
], SaleResolver);
exports.SaleResolver = SaleResolver;
//# sourceMappingURL=sale.js.map