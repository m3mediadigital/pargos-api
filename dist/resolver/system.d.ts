export declare class SystemResolver {
    querySystem(): {
        name: any;
        description: any;
        currentTime: Date;
    };
}
