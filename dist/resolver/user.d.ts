import { User } from '../model/user';
import { UserCreateInput, UserUpdateInput } from '../type/user.dto';
import { MyRequest } from '../common/request';
import { GetOneArgs, GetManyArgs } from '../model/base';
import { Session } from '../model/session';
import { PersonInput } from '../type/person.dto';
export declare class UserResolver {
    sessions(user: User): Promise<Session[]>;
    queryMe(user: User): User;
    queryUser(args: GetOneArgs): Promise<import("../model/base").BaseEntity>;
    queryUsers(args: GetManyArgs): any;
    mutationLogin(email: string, password: string, req: MyRequest): Promise<{
        token: string;
        user: User;
    }>;
    mutationUserChange(email: string, input: PersonInput, user: User): Promise<User>;
    mutationUserChangeOldPassword(user: User, oldPassword: string, newPassword: string): Promise<boolean>;
    mutationUserCreate(input: UserCreateInput): Promise<User>;
    mutationUserUpdate(args: GetOneArgs, input: UserUpdateInput): Promise<User>;
}
