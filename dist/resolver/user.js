"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserResolver = void 0;
const graphql_1 = require("@nestjs/graphql");
const user_1 = require("../model/user");
const user_dto_1 = require("../type/user.dto");
const auth_1 = require("../common/auth");
const base_1 = require("../model/base");
const session_1 = require("../model/session");
const util_1 = require("../common/util");
const messages_1 = require("../common/messages");
const debug_1 = require("../common/debug");
const person_dto_1 = require("../type/person.dto");
const debug = debug_1.default('UserResolver');
let UserResolver = class UserResolver {
    sessions(user) {
        return session_1.Session.ofUser(user);
    }
    queryMe(user) {
        return user;
    }
    queryUser(args) {
        return user_1.User.findOneByArgs(args);
    }
    queryUsers(args) {
        return user_1.User.findPage(args);
    }
    async mutationLogin(email, password, req) {
        const user = await user_1.User.findToLogin(email.trim().toLowerCase());
        if (!user)
            throw new Error(messages_1.default.loginError);
        const passwordValid = await util_1.Bcrypt.compare(password, user.password);
        if (!passwordValid)
            throw new Error(messages_1.default.loginError);
        const session = await session_1.Session.toUser(user, req.ip);
        await session.save();
        return {
            token: session.token,
            user,
        };
    }
    async mutationUserChange(email, input, user) {
        let data = {};
        if (email || input) {
            if (email) {
                data.email = email;
            }
            if (input) {
                data.person = input;
            }
        }
        else {
            return user;
        }
        await user_1.User.update({ id: user.id }, data).catch((err) => {
            debug('mutationUserChange', err);
            if (err.routine === '_bt_check_unique' &&
                err.constraint === 'UQ_78a916df40e02a9deb1c4b75edb')
                throw new Error('E-mail já cadastrado!');
            throw new Error('Erro atualizando as informações, por favor tente novamente em alguns instantes');
        });
        await user.reload();
        return user;
    }
    async mutationUserChangeOldPassword(user, oldPassword, newPassword) {
        const passwordValid = await util_1.Bcrypt.compare(oldPassword, user.password);
        if (!passwordValid)
            throw new Error(messages_1.default.wrongPassword);
        user.password = await util_1.Bcrypt.hash(newPassword, 10);
        await user.save();
        return true;
    }
    async mutationUserCreate(input) {
        if (input.password) {
            input.password = await util_1.Bcrypt.hash(input.password, 10);
        }
        return user_1.User.create(input).save();
    }
    async mutationUserUpdate(args, input) {
        const record = await user_1.User.findOne({ id: args.id });
        if (!record)
            throw new Error('Registro não encontrado!');
        if (input.password) {
            input.password = await util_1.Bcrypt.hash(input.password, 10);
        }
        await user_1.User.update({ id: record.id }, input);
        await record.reload();
        return record;
    }
};
__decorate([
    graphql_1.ResolveField(),
    auth_1.IsPublic(),
    __param(0, graphql_1.Root()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_1.User]),
    __metadata("design:returntype", void 0)
], UserResolver.prototype, "sessions", null);
__decorate([
    graphql_1.Query(() => user_1.User, { name: 'me' }),
    auth_1.IsPublic(),
    __param(0, auth_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_1.User]),
    __metadata("design:returntype", void 0)
], UserResolver.prototype, "queryMe", null);
__decorate([
    graphql_1.Query(() => user_1.User, { name: 'user' }),
    __param(0, graphql_1.Args()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [base_1.GetOneArgs]),
    __metadata("design:returntype", void 0)
], UserResolver.prototype, "queryUser", null);
__decorate([
    graphql_1.Query(() => user_dto_1.UserConnection, { name: 'users' }),
    __param(0, graphql_1.Args()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [base_1.GetManyArgs]),
    __metadata("design:returntype", void 0)
], UserResolver.prototype, "queryUsers", null);
__decorate([
    graphql_1.Mutation(() => user_dto_1.AuthPayload, { name: 'login' }),
    auth_1.IsPublic({ userRequired: false }),
    __param(0, graphql_1.Args('email')),
    __param(1, graphql_1.Args('password')),
    __param(2, graphql_1.Context('req')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Object]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "mutationLogin", null);
__decorate([
    graphql_1.Mutation(() => user_1.User, { name: 'userChange' }),
    auth_1.IsPublic(),
    __param(0, graphql_1.Args('email', { nullable: true })),
    __param(1, graphql_1.Args('input', { nullable: true })),
    __param(2, auth_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, person_dto_1.PersonInput,
        user_1.User]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "mutationUserChange", null);
__decorate([
    graphql_1.Mutation(() => Boolean, { name: 'userChangeOldPassword' }),
    auth_1.IsPublic(),
    __param(0, auth_1.CurrentUser()),
    __param(1, graphql_1.Args('oldPassword')),
    __param(2, graphql_1.Args('newPassword')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_1.User, String, String]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "mutationUserChangeOldPassword", null);
__decorate([
    graphql_1.Mutation(() => user_1.User, { name: 'userCreate' }),
    __param(0, graphql_1.Args('input')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_dto_1.UserCreateInput]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "mutationUserCreate", null);
__decorate([
    graphql_1.Mutation(() => user_1.User, { name: 'userUpdate' }),
    __param(0, graphql_1.Args()),
    __param(1, graphql_1.Args('input')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [base_1.GetOneArgs,
        user_dto_1.UserUpdateInput]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "mutationUserUpdate", null);
UserResolver = __decorate([
    graphql_1.Resolver(() => user_1.User)
], UserResolver);
exports.UserResolver = UserResolver;
//# sourceMappingURL=user.js.map