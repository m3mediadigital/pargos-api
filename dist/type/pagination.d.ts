export declare class Pagination {
    page: number;
    after?: string;
    limit: number;
    disabled: boolean;
}
export declare class PageInfo {
    count: number;
    pages: number;
    current: number;
}
