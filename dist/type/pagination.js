"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PageInfo = exports.Pagination = void 0;
const graphql_1 = require("@nestjs/graphql");
let Pagination = class Pagination {
};
__decorate([
    graphql_1.Field(() => graphql_1.Int, { nullable: true, defaultValue: 0 }),
    __metadata("design:type", Number)
], Pagination.prototype, "page", void 0);
__decorate([
    graphql_1.Field(() => graphql_1.ID, { nullable: true }),
    __metadata("design:type", String)
], Pagination.prototype, "after", void 0);
__decorate([
    graphql_1.Field(() => graphql_1.Int, { nullable: true, defaultValue: 20 }),
    __metadata("design:type", Number)
], Pagination.prototype, "limit", void 0);
__decorate([
    graphql_1.Field(() => Boolean, { nullable: true, defaultValue: false }),
    __metadata("design:type", Boolean)
], Pagination.prototype, "disabled", void 0);
Pagination = __decorate([
    graphql_1.InputType()
], Pagination);
exports.Pagination = Pagination;
let PageInfo = class PageInfo {
};
__decorate([
    graphql_1.Field(() => graphql_1.Int),
    __metadata("design:type", Number)
], PageInfo.prototype, "count", void 0);
__decorate([
    graphql_1.Field(() => graphql_1.Int),
    __metadata("design:type", Number)
], PageInfo.prototype, "pages", void 0);
__decorate([
    graphql_1.Field(() => graphql_1.Int),
    __metadata("design:type", Number)
], PageInfo.prototype, "current", void 0);
PageInfo = __decorate([
    graphql_1.ObjectType()
], PageInfo);
exports.PageInfo = PageInfo;
//# sourceMappingURL=pagination.js.map