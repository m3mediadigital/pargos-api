import { Address } from '../model/address';
declare const AddressInput_base: import("@nestjs/common").Type<Partial<Address>>;
export declare class AddressInput extends AddressInput_base {
}
export declare class PersonInput {
    name?: string;
    sex?: string;
    rg?: string;
    rgInfo?: string;
    cpf?: string;
    nationality?: string;
    birthday?: Date;
    maritalStatus?: string;
    profission?: string;
    phone?: string;
    address?: AddressInput;
}
export {};
