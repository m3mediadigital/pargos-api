import { Connection } from '../model/base';
import { Plan } from '../model/plan';
export declare class PlanConnection extends Connection {
    nodes: Plan[];
}
export declare class PlanCreateInput {
    name: string;
    value: number;
    comission: number;
    info?: string;
}
declare const PlanUpdateInput_base: import("@nestjs/common").Type<Partial<PlanCreateInput>>;
export declare class PlanUpdateInput extends PlanUpdateInput_base {
}
export {};
