"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlanUpdateInput = exports.PlanCreateInput = exports.PlanConnection = void 0;
const graphql_1 = require("@nestjs/graphql");
const base_1 = require("../model/base");
const plan_1 = require("../model/plan");
let PlanConnection = class PlanConnection extends base_1.Connection {
};
__decorate([
    graphql_1.Field(() => [plan_1.Plan]),
    __metadata("design:type", Array)
], PlanConnection.prototype, "nodes", void 0);
PlanConnection = __decorate([
    graphql_1.ObjectType({ implements: base_1.Connection })
], PlanConnection);
exports.PlanConnection = PlanConnection;
let PlanCreateInput = class PlanCreateInput {
};
__decorate([
    graphql_1.Field(),
    __metadata("design:type", String)
], PlanCreateInput.prototype, "name", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Number)
], PlanCreateInput.prototype, "value", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Number)
], PlanCreateInput.prototype, "comission", void 0);
__decorate([
    graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], PlanCreateInput.prototype, "info", void 0);
PlanCreateInput = __decorate([
    graphql_1.InputType()
], PlanCreateInput);
exports.PlanCreateInput = PlanCreateInput;
let PlanUpdateInput = class PlanUpdateInput extends graphql_1.PartialType(PlanCreateInput) {
};
PlanUpdateInput = __decorate([
    graphql_1.InputType()
], PlanUpdateInput);
exports.PlanUpdateInput = PlanUpdateInput;
//# sourceMappingURL=plan.dto.js.map