import { Connection } from '../model/base';
import { Sale } from '../model/sale';
import { PageInfo } from './pagination';
import { AddressInput, PersonInput } from './person.dto';
export declare class SaleConnection extends Connection {
    nodes: Sale[];
}
export declare class SaleComission {
    sale: Sale;
    comission: number;
    date: Date;
}
export declare class SaleComissionConnection {
    nodes: SaleComission[];
    pageInfo: PageInfo;
}
export declare class SaleComissionGraph {
    title: string;
    minX: number;
    maxX: number;
    minY: number;
    maxY: number;
    data: SaleComissionDay[];
}
export declare class SaleComissionDay {
    day: Date;
    total: number;
    sales: string[];
}
export declare class SaleCreateInput {
    planId: string;
    person?: PersonInput;
    email?: string;
    address2?: AddressInput;
    phone2?: string;
    dependents?: string[];
}
export declare class SaleUpdateInput {
    isConfirmed?: boolean;
    person?: PersonInput;
    email?: string;
    address2?: AddressInput;
    phone2?: string;
    dependents?: string[];
}
