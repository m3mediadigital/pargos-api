"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SaleUpdateInput = exports.SaleCreateInput = exports.SaleComissionDay = exports.SaleComissionGraph = exports.SaleComissionConnection = exports.SaleComission = exports.SaleConnection = void 0;
const graphql_1 = require("@nestjs/graphql");
const base_1 = require("../model/base");
const sale_1 = require("../model/sale");
const pagination_1 = require("./pagination");
const person_dto_1 = require("./person.dto");
let SaleConnection = class SaleConnection extends base_1.Connection {
};
__decorate([
    graphql_1.Field(() => [sale_1.Sale]),
    __metadata("design:type", Array)
], SaleConnection.prototype, "nodes", void 0);
SaleConnection = __decorate([
    graphql_1.ObjectType({ implements: base_1.Connection })
], SaleConnection);
exports.SaleConnection = SaleConnection;
let SaleComission = class SaleComission {
};
__decorate([
    graphql_1.Field(() => sale_1.Sale),
    __metadata("design:type", sale_1.Sale)
], SaleComission.prototype, "sale", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Number)
], SaleComission.prototype, "comission", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Date)
], SaleComission.prototype, "date", void 0);
SaleComission = __decorate([
    graphql_1.ObjectType()
], SaleComission);
exports.SaleComission = SaleComission;
let SaleComissionConnection = class SaleComissionConnection {
};
__decorate([
    graphql_1.Field(() => [SaleComission]),
    __metadata("design:type", Array)
], SaleComissionConnection.prototype, "nodes", void 0);
__decorate([
    graphql_1.Field(() => pagination_1.PageInfo),
    __metadata("design:type", pagination_1.PageInfo)
], SaleComissionConnection.prototype, "pageInfo", void 0);
SaleComissionConnection = __decorate([
    graphql_1.ObjectType()
], SaleComissionConnection);
exports.SaleComissionConnection = SaleComissionConnection;
let SaleComissionGraph = class SaleComissionGraph {
};
__decorate([
    graphql_1.Field(),
    __metadata("design:type", String)
], SaleComissionGraph.prototype, "title", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Number)
], SaleComissionGraph.prototype, "minX", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Number)
], SaleComissionGraph.prototype, "maxX", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Number)
], SaleComissionGraph.prototype, "minY", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Number)
], SaleComissionGraph.prototype, "maxY", void 0);
__decorate([
    graphql_1.Field(() => [SaleComissionDay]),
    __metadata("design:type", Array)
], SaleComissionGraph.prototype, "data", void 0);
SaleComissionGraph = __decorate([
    graphql_1.ObjectType()
], SaleComissionGraph);
exports.SaleComissionGraph = SaleComissionGraph;
let SaleComissionDay = class SaleComissionDay {
};
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Date)
], SaleComissionDay.prototype, "day", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Number)
], SaleComissionDay.prototype, "total", void 0);
__decorate([
    graphql_1.Field(() => [graphql_1.ID]),
    __metadata("design:type", Array)
], SaleComissionDay.prototype, "sales", void 0);
SaleComissionDay = __decorate([
    graphql_1.ObjectType()
], SaleComissionDay);
exports.SaleComissionDay = SaleComissionDay;
let SaleCreateInput = class SaleCreateInput {
};
__decorate([
    graphql_1.Field(() => graphql_1.ID),
    __metadata("design:type", String)
], SaleCreateInput.prototype, "planId", void 0);
__decorate([
    graphql_1.Field(() => person_dto_1.PersonInput, { nullable: true }),
    __metadata("design:type", person_dto_1.PersonInput)
], SaleCreateInput.prototype, "person", void 0);
__decorate([
    graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], SaleCreateInput.prototype, "email", void 0);
__decorate([
    graphql_1.Field(() => person_dto_1.AddressInput, { nullable: true }),
    __metadata("design:type", person_dto_1.AddressInput)
], SaleCreateInput.prototype, "address2", void 0);
__decorate([
    graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], SaleCreateInput.prototype, "phone2", void 0);
__decorate([
    graphql_1.Field(() => [String], { nullable: true }),
    __metadata("design:type", Array)
], SaleCreateInput.prototype, "dependents", void 0);
SaleCreateInput = __decorate([
    graphql_1.InputType()
], SaleCreateInput);
exports.SaleCreateInput = SaleCreateInput;
let SaleUpdateInput = class SaleUpdateInput {
};
__decorate([
    graphql_1.Field({ nullable: true }),
    __metadata("design:type", Boolean)
], SaleUpdateInput.prototype, "isConfirmed", void 0);
__decorate([
    graphql_1.Field(() => person_dto_1.PersonInput, { nullable: true }),
    __metadata("design:type", person_dto_1.PersonInput)
], SaleUpdateInput.prototype, "person", void 0);
__decorate([
    graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], SaleUpdateInput.prototype, "email", void 0);
__decorate([
    graphql_1.Field(() => person_dto_1.AddressInput, { nullable: true }),
    __metadata("design:type", person_dto_1.AddressInput)
], SaleUpdateInput.prototype, "address2", void 0);
__decorate([
    graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], SaleUpdateInput.prototype, "phone2", void 0);
__decorate([
    graphql_1.Field(() => [String], { nullable: true }),
    __metadata("design:type", Array)
], SaleUpdateInput.prototype, "dependents", void 0);
SaleUpdateInput = __decorate([
    graphql_1.InputType()
], SaleUpdateInput);
exports.SaleUpdateInput = SaleUpdateInput;
//# sourceMappingURL=sale.dto.js.map