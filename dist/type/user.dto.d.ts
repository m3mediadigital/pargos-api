import { Connection } from '../model/base';
import { User } from '../model/user';
import { PersonInput } from './person.dto';
export declare class UserConnection extends Connection {
    nodes: User[];
}
export declare class AuthPayload {
    token: string;
    user: User;
}
export declare class UserCreateInput {
    email: string;
    password: string;
    role: string;
    person: PersonInput;
}
declare const UserUpdateInput_base: import("@nestjs/common").Type<Partial<UserCreateInput>>;
export declare class UserUpdateInput extends UserUpdateInput_base {
}
export {};
