"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserUpdateInput = exports.UserCreateInput = exports.AuthPayload = exports.UserConnection = void 0;
const graphql_1 = require("@nestjs/graphql");
const base_1 = require("../model/base");
const user_1 = require("../model/user");
const person_dto_1 = require("./person.dto");
let UserConnection = class UserConnection extends base_1.Connection {
};
__decorate([
    graphql_1.Field(() => [user_1.User]),
    __metadata("design:type", Array)
], UserConnection.prototype, "nodes", void 0);
UserConnection = __decorate([
    graphql_1.ObjectType({ implements: base_1.Connection })
], UserConnection);
exports.UserConnection = UserConnection;
let AuthPayload = class AuthPayload {
};
__decorate([
    graphql_1.Field(),
    __metadata("design:type", String)
], AuthPayload.prototype, "token", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", user_1.User)
], AuthPayload.prototype, "user", void 0);
AuthPayload = __decorate([
    graphql_1.ObjectType()
], AuthPayload);
exports.AuthPayload = AuthPayload;
let UserCreateInput = class UserCreateInput {
};
__decorate([
    graphql_1.Field(),
    __metadata("design:type", String)
], UserCreateInput.prototype, "email", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", String)
], UserCreateInput.prototype, "password", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", String)
], UserCreateInput.prototype, "role", void 0);
__decorate([
    graphql_1.Field(() => person_dto_1.PersonInput),
    __metadata("design:type", person_dto_1.PersonInput)
], UserCreateInput.prototype, "person", void 0);
UserCreateInput = __decorate([
    graphql_1.InputType()
], UserCreateInput);
exports.UserCreateInput = UserCreateInput;
let UserUpdateInput = class UserUpdateInput extends graphql_1.PartialType(UserCreateInput) {
};
UserUpdateInput = __decorate([
    graphql_1.InputType()
], UserUpdateInput);
exports.UserUpdateInput = UserUpdateInput;
//# sourceMappingURL=user.dto.js.map