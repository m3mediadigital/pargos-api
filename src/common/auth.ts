import {
	createParamDecorator,
	CanActivate,
	ExecutionContext,
	Injectable,
	SetMetadata,
	applyDecorators,
	UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Session } from '../model/session';
import { getRequest, getResponse } from './request';
import { GqlContextType, GqlExecutionContext } from '@nestjs/graphql';
import Messages from './messages';

export const CurrentUser = createParamDecorator(
	(prop: string, ctx: ExecutionContext) => {
		const req = getRequest(ctx);
		return prop ? req.currentUser && req.currentUser[prop] : req.currentUser;
	},
);

export const CurrentSession = createParamDecorator(
	(_, ctx: ExecutionContext) => {
		const req = getRequest(ctx);
		return req.session;
	},
);

export async function AuthMiddleware(req, res: Response, next: Function) {
	try {
		req.headers['authorization'] = req.headers['authorization']?.replace(
			'Bearer ',
			'',
		);
		if (req.headers['authorization']) {
			req.session = await Session.createQueryBuilder('session')
				.innerJoinAndSelect('session.user', 'user')
				.where(
					`session.token = :token AND ((session.updated > NOW() - INTERVAL '6 hours') OR (session.expirable = FALSE))`,
					{
						token: req.headers['authorization'],
					},
				)
				.getOne();
			if (req.session) {
				await req.session.touch();
				req.currentUser = req.session.user;
			}
		}
	} catch (ignore) {}
	next();
}

//export function Field({ type = 'text' } = {}) {
//	return applyDecorators(
//		Column(type) as PropertyDecorator,
//		GField(() => type),
//	);
//}

export function IsPublic({ userRequired = true } = {}) {
	return applyDecorators(
		SetMetadata('userRequired', userRequired),
		SetMetadata('public', true),
	);
}

export function IsPrivate() {
	return applyDecorators(
		SetMetadata('userRequired', true),
		SetMetadata('public', false),
	);
}

export function isResolvingGraphQLField(context: ExecutionContext): boolean {
	if (context.getType<GqlContextType>() === 'graphql') {
		const gqlContext = GqlExecutionContext.create(context);
		const info = gqlContext.getInfo();
		const parentType = info.parentType.name;
		return parentType !== 'Query' && parentType !== 'Mutation';
	}
	return false;
}

@Injectable()
export class AuthGuard implements CanActivate {
	constructor(private readonly reflector: Reflector) {}

	async canActivate(ctx: ExecutionContext) {
		// const resolvinField = isResolvingGraphQLField(ctx);
		const req = getRequest(ctx);

		const userRequired = !!this.reflector.getAllAndOverride<boolean>(
			'userRequired',
			[ctx.getClass(), ctx.getHandler()],
		);
		if (userRequired && !req.currentUser) {
			const res = getResponse(ctx);
			res.status(401);
			throw new UnauthorizedException(Messages.notAuthenticated);
		}

		const isPublic = !!this.reflector.getAllAndOverride<boolean>('public', [
			ctx.getClass(),
			ctx.getHandler(),
		]);
		if (isPublic) return true;

		const userPermission = req.currentUser.role;
		if (!userPermission) return false;

		const superResource = ctx
			.getClass()
			.name.replace(/Resolver$/, '')
			.toLowerCase();
		const resource = [superResource, ctx.getHandler().name]
			.join('.')
			.toLowerCase();

		if (userPermission === 'root') return true;
		if (userPermission === superResource) return true;
		return userPermission === resource;
	}
}
