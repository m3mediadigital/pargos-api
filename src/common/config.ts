import Util, { Path } from './util';
import { AuthMiddleware } from './auth';

const configs = Util.getConfigs();

configs.db = {
	type: 'postgres',
	application_name: configs.name,
	...configs.db,
	ssl: process.env.NODE_ENV === 'production' ? {
		rejectUnauthorized: false,
	} : null,
	entities: [Path.join(__dirname, '..', 'model', '*{.ts,.js}')],
	migrations: [Path.join(__dirname, '..', 'migration', '*{.ts,.js}')],
	migrationsRun: true,
	cli: {
		entitiesDir: Path.join(process.cwd(), 'src', 'model'),
		subscribersDir: Path.join(process.cwd(), 'src', 'subscriber'),
		migrationsDir: Path.join(process.cwd(), 'src', 'migration'),
	},
};

configs.api = {
	autoSchemaFile: 'schema.gql',
	fieldResolverEnhancers: ['guards'],
	...configs.api,
	context({ req, res, connection }) {
		return {
			req: connection ? connection.context : req,
			res,
		};
	},
	subscriptions: {
		onConnect(connectionParams, _, ctx) {
			ctx.request.headers['authorization'] =
				connectionParams['Authorization'] || connectionParams['authorization'];
			return new Promise((resolve) => {
				AuthMiddleware(ctx.request, null, () => {
					resolve(ctx.request);
				});
			});
		},
	},
};

export default configs;
