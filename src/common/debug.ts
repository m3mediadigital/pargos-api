import Debug from 'debug';

export default function debug(...modulesNames: string[]) {
	return Debug([process.env.npm_package_name, ...modulesNames].join(':'));
}
