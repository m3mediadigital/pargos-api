import { createTransport, SendMailOptions } from 'nodemailer';
// import InlineBase64 from 'nodemailer-plugin-inline-base64';
import { htmlToText } from 'nodemailer-html-to-text';
import { render as RenderMail, Options as RenderMailOptions } from 'ejs';
import configs from './config';

const Mail = createTransport(configs.email.transport);

// Mail.use('compile', InlineBase64());
Mail.use('compile', htmlToText());

export { Mail, SendMailOptions, RenderMail, RenderMailOptions };
