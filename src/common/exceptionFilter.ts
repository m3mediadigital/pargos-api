import { Catch, ArgumentsHost, ExceptionFilter } from '@nestjs/common';

@Catch()
export class ExceptionsFilter implements ExceptionFilter {
	catch(exception: unknown, host: ArgumentsHost) {
		throw exception;
	}
}
