import { ArgumentsHost } from '@nestjs/common';
import { GqlArgumentsHost } from '@nestjs/graphql';
import { Session } from '../model/session';
import { User } from '../model/user';

export interface MyRequest extends Request {
	ip: string;
	session: Session;
	currentUser: User;
}

export function getRequest(ctx: ArgumentsHost) {
	if (ctx.getType().toString() === 'graphql' || !!ctx.getArgByIndex(3)) {
		const req = GqlArgumentsHost.create(ctx).getContext().req;
		return req.raw || req;
	} else {
		return ctx.switchToHttp().getRequest();
	}
}

export function getResponse(ctx: ArgumentsHost) {
	if (ctx.getType().toString() === 'graphql' || !!ctx.getArgByIndex(3)) {
		return GqlArgumentsHost.create(ctx).getContext().res;
	} else {
		return ctx.switchToHttp().getResponse();
	}
}
