import * as URL from 'url';
import * as HTTP from 'http';
import * as Path from 'path';
import * as FS from 'fs';
import * as Crypto from 'crypto';
import * as Bcrypt from 'bcrypt';
import * as UUID from 'uuid';
import * as JS from 'util';
import deepmerge from 'deepmerge';
import * as YAML from 'js-yaml';

type RequestOptions = HTTP.RequestOptions & {
	url: string;
	body?: any;
	raw?: boolean;
};

export class RequestResult extends HTTP.IncomingMessage {
	constructor(res: HTTP.IncomingMessage) {
		super(res.socket);
	}

	receiveBody(): Promise<any[]> {
		return new Promise((resolve, reject) => {
			let rawData = [];
			this.on('data', (chunk) => {
				rawData.push(chunk);
			});
			this.on('end', () => {
				resolve(rawData);
			});
			this.on('error', (err) => reject(err));
		});
	}

	async toText(encoding: BufferEncoding = 'utf8'): Promise<string> {
		this.setEncoding(encoding);
		const result = await this.receiveBody();
		return result.join('');
	}

	async toJson(encoding: BufferEncoding = 'utf8'): Promise<any> {
		this.setEncoding(encoding);
		const result = await this.receiveBody();
		return JSON.parse(result.join(''));
	}
}

export default class Util {
	static get js() {
		return {
			...JS,

			merge: deepmerge,

			/**
			 * Merge the property descriptors of `src` into `dest`
			 *
			 * @param {object} dest Object to add descriptors to
			 * @param {object} src Object to clone descriptors from
			 * @param {boolean} [redefine=true] Redefine `dest` properties with `src` properties
			 * @returns {object} Reference to dest
			 * @public
			 */
			mergeObjects(dest, src, redefine = true) {
				if (!dest) {
					throw new TypeError('argument dest is required');
				}

				if (!src) {
					throw new TypeError('argument src is required');
				}

				const hasOwnProperty = Object.prototype.hasOwnProperty;

				Object.getOwnPropertyNames(src).forEach(function forEachOwnPropertyName(
					name,
				) {
					if (!redefine && hasOwnProperty.call(dest, name)) {
						// Skip desriptor
						return;
					}

					// Copy descriptor
					const descriptor = Object.getOwnPropertyDescriptor(src, name);
					Object.defineProperty(dest, name, descriptor);
				});

				return dest;
			},

			mixClasses(baseClass, ...mixins) {
				const base = class _Combined extends baseClass {
					constructor(...args) {
						super(...args);
						mixins.forEach((mixin) => {
							mixin.prototype.initializer.call(this);
						});
					}
				};
				const copyProps = (target, source) => {
					Object.getOwnPropertyNames(source)
						// .concat(Object.getOwnPropertySymbols(source))
						.forEach((prop) => {
							if (
								prop.match(
									/^(?:constructor|prototype|arguments|caller|name|bind|call|apply|toString|length)$/,
								)
							)
								return;
							Object.defineProperty(
								target,
								prop,
								Object.getOwnPropertyDescriptor(source, prop),
							);
						});
				};
				mixins.forEach((mixin) => {
					copyProps(base.prototype, mixin.prototype);
					copyProps(base, mixin);
				});
				return base;
			},

			/**
			 * @template T
			 * @param {Promise<T>} promise
			 * @returns {Promise<{result?:T,status:String,error?:Error}>}
			 */
			promiseReflect(promise) {
				return promise.then(
					(result) => ({ result, status: 'fulfilled' }),
					(error) => ({ error, status: 'rejected' }),
				);
			},

			arrayGroupBy(array, key) {
				return array.reduce((m, v) => {
					(m[v[key]] = m[v[key]] || []).push(v);
					return m;
				}, {});
			},

			arrayLast(array) {
				return array[array.length - 1];
			},

			eventToPromise(events, resolveName, rejectName = 'error') {
				return new Promise((resolve, reject) => {
					events.on(resolveName, resolve);
					events.on(rejectName, reject);
				});
			},
		};
	}

	static get SchemaYAML() {
		const EnvYamlType = new YAML.Type('!env', {
			kind: 'scalar',
			construct(data) {
				const result = data !== null ? process.env[data] : '';
				return result === 'true' ? true : result;
			},
		});
		const EvalYamlType = new YAML.Type('!eval', {
			kind: 'scalar',
			construct(data) {
				return data !== null ? eval(data) : '';
			},
		});
		const OrYamlType = new YAML.Type('!or', {
			kind: 'sequence',
			construct(data) {
				if (data === null) return null;
				return data.reduce((m, v) => m || v, null);
			},
		});
		return new YAML.Schema([EnvYamlType, EvalYamlType, OrYamlType]);
	}

	/**
	 * Parse configs from files
	 */
	static getConfigs(): any {
		const schema = Util.SchemaYAML;

		const pathConfigDefault = Path.resolve(
			process.cwd(),
			'config.default.yaml',
		);
		let configs = YAML.load(FS.readFileSync(pathConfigDefault, 'utf8'), {
			schema,
		});

		if (process.env.NODE_ENV === 'production') {
			const pathConfigProd = Path.resolve(process.cwd(), 'config.prod.yaml');
			if (FS.existsSync(pathConfigProd)) {
				const configsProd = YAML.load(FS.readFileSync(pathConfigProd, 'utf8'), {
					schema,
				});
				if (configsProd) configs = deepmerge(configs as Object, configsProd as Object);
			}
		}

		const pathConfigLocal = Path.resolve(process.cwd(), 'config.local.yaml');
		if (FS.existsSync(pathConfigLocal)) {
			const configsLocal = YAML.load(FS.readFileSync(pathConfigLocal, 'utf8'), {
				schema,
			});
			if (configsLocal) configs = deepmerge(configs as Object, configsLocal as Object);
		}

		return configs;
	}

	/**
	 * Parse messages from files
	 */
	static getMessages(): any {
		const schema = Util.SchemaYAML;

		const pathFile = Path.resolve(
			process.cwd(),
			'src',
			'locales',
			'default.yaml',
		);
		return YAML.load(FS.readFileSync(pathFile, 'utf8'), { schema });
	}
}

export { Path, FS, URL, Crypto, Bcrypt, UUID };
