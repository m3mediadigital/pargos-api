import { NestFactory } from '@nestjs/core';
import 'reflect-metadata';
import { AppModule } from './module';
import { AuthMiddleware } from './common/auth';
import Config from './common/config';

async function bootstrap() {
	// const app = await NestFactory.create<NestFastifyApplication>(
	// 	AppModule,
	// 	new FastifyAdapter(Config.server),
	// );
	const app = await NestFactory.create(AppModule);
	app.enableCors(Config.cors);
	app.use(AuthMiddleware);
	await app.listen(Config.port, Config.host);
}
bootstrap();
