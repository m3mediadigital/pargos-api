import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateRootUser1584217047347 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		return queryRunner.query(`INSERT INTO "user"
			("email", "password", "role") VALUES
			('user@email.com', '$2b$10$2Vlm.WzHZ32PRO3V.VRHcuw6BMkRlnBIh0iBZ9j/QAGTMq5ed6tWS','root')
		;`);
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		return queryRunner.query(`
			DELETE FROM "user" CASCATE
			WHERE "email" = 'user@email.com'
		;`);
	}
}
