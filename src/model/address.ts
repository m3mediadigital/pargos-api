import { Column } from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Address {
	@Column('text', { nullable: true })
	@Field({ nullable: true })
	street: string;

	@Column('text', { nullable: true })
	@Field({ nullable: true })
	cep: string;

	@Column('text', { nullable: true })
	@Field({ nullable: true })
	district: string;

	@Column('text', { nullable: true })
	@Field({ nullable: true })
	city: string;

	@Column('text', { nullable: true })
	@Field({ nullable: true })
	uf: string;
}
