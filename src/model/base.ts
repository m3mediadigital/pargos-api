import {
	BaseEntity as OrmEntity,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
	ObjectType as TObjectType,
	ObjectLiteral,
	OrderByCondition,
	DeleteDateColumn,
	getConnection,
} from 'typeorm';
import { Field, ID, InterfaceType, ArgsType } from '@nestjs/graphql';
import { GraphQLScalarType } from 'graphql';
import { Pagination, PageInfo } from '../type/pagination';

export class BaseEntity extends OrmEntity {
	static findOneByArgs<T extends BaseEntity>(args: GetOneArgs) {
		return this.findOne<T>(args.id);
	}

	static findPage<T extends BaseEntity>(
		this: TObjectType<T>,
		args: GetManyArgs,
	);
	static findPage<T extends BaseEntity>(
		this: TObjectType<T>,
		options?: FindPageOptions<T>,
		args?: GetManyArgs,
	);
	static async findPage<T extends BaseEntity>(this: TObjectType<T>, ...params) {
		let options: FindPageOptions<T> = {};
		let args: GetManyArgs;
		if (params.length === 1) {
			[args] = params;
		} else {
			[options, args] = params;
		}

		const current =
			(args.pagination.after
				? parseInt(args.pagination.after)
				: args.pagination.page) || 0;
		const query = getConnection().createQueryBuilder(this, this.name);

		if (!args.pagination.disabled) {
			query.take(args.pagination.limit);
		}

		query.skip(current * args.pagination.limit);

		if (options.where) {
			query.where(options.where);
		}
		if (options.parameters) {
			query.setParameters(options.parameters);
		}
		if (options.order) {
			query.orderBy(options.order);
		}
		if (args.q && Reflect.hasMetadata('queryKeys', this)) {
			const keys: string[] = Reflect.getMetadata('queryKeys', this);
			//const inners: string[] = [];
			const keysParsed = keys
				.map((key) => {
					//if (key.includes('.')) {
					//	const [entity, prop] = key.split('.');
					//	if (!inners.includes(entity)) {
					//		inners.push(entity);
					//	}
					//	return `COALESCE("${entity}"."${prop}"::TEXT)`;
					//}
					return `COALESCE("${this.name}"."${key}"::TEXT)`;
				})
				.filter((key) => !!key);
			//inners.forEach(entity => query.innerJoin(entity, entity, `"${entity}"."id" = "${this.name}"."${entity}Id"`));
			if (keysParsed.length > 0) {
				query.andWhere(
					`TO_TSVECTOR('portuguese', ${keysParsed.join(
						` || ' ' || `,
					)}) @@ WEBSEARCH_TO_TSQUERY('portuguese', :q)`,
				);
				query.orderBy(
					`TS_RANK_CD(TO_TSVECTOR('portuguese', ${keysParsed.join(
						` || ' ' || `,
					)}), WEBSEARCH_TO_TSQUERY('portuguese', :q))`,
				);
				query.setParameter('q', args.q);
			}
		}

		//const [nodes, count] = await super.findAndCount({
		//	skip: current * args.pagination.limit,
		//	take: args.pagination.limit,
		//	where,
		//	...options,
		//});
		const [nodes, count] = await query.getManyAndCount();

		return {
			nodes,
			pageInfo: {
				count,
				pages: Math.ceil(count / args.pagination.limit),
				current,
			},
		};
	}
}

@InterfaceType()
export abstract class Node extends BaseEntity {
	@PrimaryGeneratedColumn({ type: 'bigint' })
	@Field(() => ID)
	id: string;

	@CreateDateColumn()
	@Field()
	createdAt: Date;

	@UpdateDateColumn()
	@Field()
	updatedAt: Date;

	@DeleteDateColumn()
	deletedAt: Date;
}

export const Json = new GraphQLScalarType({
	name: 'JSON',
	serialize(value) {
		return value; // value sent to the client
	},
});

@InterfaceType()
export abstract class Payload {
	@Field(() => [Node])
	result: Node[];
}

@InterfaceType()
export abstract class Connection {
	@Field(() => [Node])
	nodes: Node[];

	@Field(() => PageInfo)
	pageInfo: PageInfo;
}

@ArgsType()
export class GetOneArgs {
	@Field(() => ID)
	id: string;
}

@ArgsType()
export class GetManyArgs {
	@Field({ nullable: true })
	q?: string;

	@Field({ nullable: true, defaultValue: { limit: 20 } })
	pagination: Pagination;
}

export interface FindPageOptions<Entity = any> {
	where?: string | ((qb: this) => string) | ObjectLiteral | ObjectLiteral[];
	parameters?: ObjectLiteral;
	order?: OrderByCondition;
}
