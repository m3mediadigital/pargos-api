import { Column } from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';
import { Address } from './address';

@ObjectType()
export class Person {
	@Column('text', { nullable: true })
	@Field({ nullable: true })
	name: string;

	@Column('text', { nullable: true })
	@Field({ nullable: true })
	sex: string;

	@Column('text', { nullable: true })
	@Field({ nullable: true })
	rg: string;

	@Column('text', { nullable: true })
	@Field({ nullable: true })
	rgInfo: string;

	@Column('text', { nullable: true })
	@Field({ nullable: true })
	cpf: string;

	@Column('text', { nullable: true })
	@Field({ nullable: true })
	nationality: string;

	@Column({ nullable: true })
	@Field({ nullable: true })
	birthday: Date;

	@Column('text', { nullable: true })
	@Field({ nullable: true })
	maritalStatus: string;

	@Column('text', { nullable: true })
	@Field({ nullable: true })
	profission: string;

	@Column(() => Address)
	@Field()
	address: Address;

	@Column('text', { nullable: true })
	@Field({ nullable: true })
	phone: string;
}
