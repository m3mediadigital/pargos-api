import { Column, Entity, OneToMany } from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';
import { SetMetadata } from '@nestjs/common';
import { Node } from './base';
import { Sale } from './sale';

@Entity({
	orderBy: { id: 'ASC' },
})
@ObjectType({ implements: Node })
@SetMetadata('queryKeys', ['name'])
export class Plan extends Node {
	@Column('text')
	@Field()
	name: string;

	@Column('numeric')
	@Field()
	value: number;

	@Column('numeric')
	@Field()
	comission: number;

	@Column('text', { nullable: true })
	@Field({ nullable: true })
	info?: string;

	@OneToMany(() => Sale, (sale) => sale.plan)
	// @Field(() => [Sale])
	sales: Sale[];
}
