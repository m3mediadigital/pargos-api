import { Entity, Column, ManyToOne, RelationId } from 'typeorm';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { SetMetadata } from '@nestjs/common';
import { Node } from './base';
import { User } from './user';
import { Address } from './address';
import { Plan } from './plan';
import { Person } from './person';

@Entity({
	orderBy: { id: 'DESC' },
})
@ObjectType({ implements: Node })
@SetMetadata('queryKeys', ['number'])
export class Sale extends Node {
	@ManyToOne(() => User, (user) => user.sales)
	@Field(() => User)
	user: User;

	@RelationId((sale: Sale) => sale.user)
	@Field(() => ID)
	userId: string;

	@ManyToOne(() => Plan, (plan) => plan.sales, { eager: true })
	@Field(() => Plan)
	plan: Plan;
	
	@RelationId((sale: Sale) => sale.plan)
	@Field(() => ID)
	planId: string;

	@Column(() => Person)
	@Field(() => Person)
	person: Person;

	@Column('text', { nullable: true })
	@Field({ nullable: true })
	email: string;

	@Column(() => Address)
	@Field(() => Address)
	address2: Address;

	@Column('text', { nullable: true })
	@Field({ nullable: true })
	phone2: string;

	@Column('simple-array', { nullable: true })
	@Field(() => [String], { nullable: true })
	dependents: string[];

	@Column({ nullable: true })
	@Field({ nullable: true })
	confirmed: Date;
}
