import {
	BaseEntity,
	Entity,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	ManyToOne,
	UpdateDateColumn,
	Column,
	Index,
} from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';
import { User } from './user';

@Entity({
	orderBy: { updated: 'DESC' },
})
@Index(['token', 'updated'])
@ObjectType()
export class Session extends BaseEntity {
	@PrimaryGeneratedColumn('uuid')
	// @Field() -- Não expor na api
	token: string;

	@Column('text')
	ip: string;

	@ManyToOne(() => User, (user) => user.sessions)
	// @Field(() => User)
	user: User;

	@CreateDateColumn()
	@Field()
	created: Date;

	@UpdateDateColumn()
	@Field()
	updated: Date;

	@Column({ nullable: false, default: false })
	@Field({ nullable: false, defaultValue: false })
	expirable: boolean;

	touch() {
		return Session.update(this.token, {});
	}

	static async ofUser(user: User): Promise<Session[]> {
		return user.sessions || Session.find({ user });
	}

	static async toUser(user: User, ip: string): Promise<Session> {
		return Session.create({ user, ip });
	}
}
