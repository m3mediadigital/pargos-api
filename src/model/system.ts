import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
export class System {
	@Field()
	name: string;

	@Field()
	description: string;

	@Field()
	currentTime: Date;
}
