import { Entity, Column, OneToMany } from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';
import { SetMetadata } from '@nestjs/common';
import { Node } from './base';
import { Session } from './session';
import { Person } from './person';
import { Sale } from './sale';

@Entity({
	orderBy: { id: 'ASC' },
})
@ObjectType({ implements: Node })
@SetMetadata('queryKeys', ['email', 'name'])
export class User extends Node {
	@Column('text', { unique: true })
	@Field()
	email: string;

	@Column('text', { select: false })
	password: string;

	@Column('text')
	@Field()
	role: string;

	@Column(() => Person)
	@Field({ nullable: true })
	person: Person;

	@OneToMany(() => Session, (session) => session.user)
	@Field(() => [Session])
	sessions: Session[];

	@OneToMany(() => Sale, (sale) => sale.user)
	@Field(() => [Sale])
	sales: Sale[];

	static findToLogin(value: any) {
		return User.createQueryBuilder('user')
			.addSelect('user.password')
			.where({ email: value })
			.getOne();
	}
}
