import { APP_GUARD } from '@nestjs/core';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GraphQLModule } from '@nestjs/graphql';
import { AuthGuard } from './common/auth';
import Config from './common/config';

import { SystemResolver } from './resolver/system';
import { UserResolver } from './resolver/user';
import { PlanResolver } from './resolver/plan';
import { SaleResolver } from './resolver/sale';

@Module({
	providers: [
		// { provide: APP_FILTER, useClass: ExceptionsFilter },
		{ provide: APP_GUARD, useClass: AuthGuard },
		// { provide: APP_INTERCEPTOR, useClass: RoleInterceptor },
	],
	imports: [
		TypeOrmModule.forRoot(Config.db),
		GraphQLModule.forRoot(Config.api),
		SystemResolver,
		UserResolver,
		PlanResolver,
		SaleResolver,
	],
})
export class AppModule { }
