import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { IsPublic } from '../common/auth';
import { GetOneArgs, GetManyArgs } from '../model/base';
import { Plan } from '../model/plan';
import {
	PlanConnection,
	PlanCreateInput,
	PlanUpdateInput,
} from '../type/plan.dto';

@Resolver(() => Plan)
export class PlanResolver {
	@Query(() => Plan, { name: 'plan' })
	@IsPublic({ userRequired: false })
	queryPlan(
		@Args() args: GetOneArgs,
	) {
		return Plan.findOne(args);
	}

	@Query(() => PlanConnection, { name: 'plans' })
	@IsPublic({ userRequired: false })
	queryPlans(
		@Args() args: GetManyArgs,
	) {
		return Plan.findPage(args);
	}

	@Mutation(() => Plan, { name: 'planCreate' })
	async mutationPlanCreate(
		@Args('input') input: PlanCreateInput,
	) {
		return Plan.create(input).save();
	}

	@Mutation(() => Plan, { name: 'planUpdate' })
	async mutationPlanUpdate(
		@Args() args: GetOneArgs,
		@Args('input') input: PlanUpdateInput,
	) {
		const plan = await Plan.findOne({ id: args.id });
		if (!plan) throw new Error('Registro não encontrado!');

		await Plan.update({ id: plan.id }, input);
		await plan.reload();

		return plan;
	}
}
