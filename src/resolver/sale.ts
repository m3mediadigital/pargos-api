import { getConnection, Not, IsNull } from 'typeorm';
import { Args, Mutation, Query, Resolver, Int, ResolveField, Root, ID, Float } from '@nestjs/graphql';
import { CurrentUser, IsPublic } from '../common/auth';
import { User } from '../model/user';
import { Sale } from '../model/sale';
import { GetOneArgs, GetManyArgs } from '../model/base';
import {
	SaleComissionConnection,
	SaleComissionGraph,
	SaleConnection,
	SaleCreateInput,
	SaleUpdateInput,
} from '../type/sale.dto';
import { Plan } from '../model/plan';

const monthNames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];

@Resolver(() => Sale)
export class SaleResolver {
	@ResolveField()
	user(@Root() sale: Sale) {
		return getConnection()
			.createQueryBuilder()
			.relation(Sale, 'user')
			.of(sale)
			.loadOne();
	}

	@ResolveField()
	plan(@Root() sale: Sale) {
		return getConnection()
			.createQueryBuilder()
			.relation(Sale, 'plan')
			.of(sale)
			.loadOne();
	}

	@ResolveField()
	isConfirmed(@Root() sale: Sale): boolean {
		return sale.confirmed != null;
	}


	@Query(() => Sale, { name: 'sale' })
	@IsPublic()
	querySale(
		@Args() args: GetOneArgs,
		@Args({ name: 'admin', nullable: true, defaultValue: false }) admin: boolean,
		@CurrentUser() user: User,
	) {
		if (admin && user.role === 'root') {
			return Sale.findOne({ id: args.id });
		} else {
			return Sale.findOne({ id: args.id, user });
		}
	}

	@Query(() => SaleConnection, { name: 'sales' })
	@IsPublic()
	querySales(
		@Args() args: GetManyArgs,
		@Args({ name: 'admin', nullable: true, defaultValue: false }) admin: boolean,
		@Args({ name: 'userId', type: () => ID, nullable: true }) saler: string,
		@CurrentUser() user: User,
	) {
		if (admin && user.role === 'root') {
			if (saler) {
				return Sale.findPage({ where: "Sale.userId = :saler", parameters: { saler } }, args);
			} else {
				return Sale.findPage(args);
			}
		} else {
			return Sale.findPage({ where: { user } }, args);
		}
	}

	@Query(() => SaleComissionConnection, { name: 'saleComissions' })
	@IsPublic()
	async queryComissions(
		@Args() args: GetManyArgs,
		@CurrentUser() user: User,
	) {
		const [sales, plans] = await Promise.all([
			Sale.findPage(
				{
					where: {
						user,
						confirmed: Not(IsNull()),
					},
				},
				args,
			),
			Plan.find().then(plans => Object.fromEntries(plans.map(plan => [plan.id, plan]))),
		]);

		return {
			nodes: sales.nodes.map(sale => {
				const plan = plans[sale.planId];
				return {
					sale,
					comission: plan.value * plan.comission,
					date: sale.confirmed,
				};
			}),
			pageInfo: sales.pageInfo,
		};
	}

	@Query(() => Int, { name: 'saleTotal' })
	@IsPublic()
	queryTotal(
		@CurrentUser() user: User,
	) {
		return Sale.count({ user });
	}

	@Query(() => Int, { name: 'saleTotalComplete' })
	@IsPublic()
	queryTotalComplete(
		@CurrentUser() user: User,
	) {
		return Sale.count({ user, confirmed: Not(IsNull()) });
	}

	@Query(() => Float, { name: 'saleValueIncoming' })
	@IsPublic()
	queryValueIncoming(
		@CurrentUser() user: User,
	) {
		return getConnection().query(`
			SELECT SUM(p.value * p.comission) AS total
			FROM "sale" s
			INNER JOIN "plan" p ON p.id = s."planId"
			WHERE s."userId" = $1 AND s.confirmed IS NOT NULL AND DATE_TRUNC('month', s."createdAt") = DATE_TRUNC('month', CURRENT_DATE);
		`, [user.id]).then(([{ total }]) => total);
	}

	@Query(() => Float, { name: 'saleValueTotal' })
	@IsPublic()
	queryValueTotal(
		@CurrentUser() user: User,
	) {
		return getConnection().query(`
			SELECT SUM(p.value * p.comission) AS total
			FROM "sale" s
			INNER JOIN "plan" p ON p.id = s."planId"
			WHERE s."userId" = $1 AND s.confirmed IS NOT NULL;
		`, [user.id]).then(([{ total }]) => total);
	}

	@Query(() => SaleComissionGraph, { name: 'saleGraph', nullable: true })
	@IsPublic()
	async queryGraph(
		@CurrentUser() user: User,
	) {
		const data = await getConnection().query(`
			SELECT d::DATE AS day, COALESCE(SUM(p.value * p.comission), 0) AS total, ARRAY_AGG(s.id) AS sales
			FROM generate_series(DATE_TRUNC('month', CURRENT_DATE)::DATE, (DATE_TRUNC('month', CURRENT_DATE) + INTERVAL '1 month' - INTERVAL '1 day')::DATE, '1 day'::INTERVAL) d
			LEFT JOIN "sale" s ON DATE_TRUNC('day', s."createdAt") = d AND s."userId" = $1 AND s.confirmed IS NOT NULL
			LEFT JOIN "plan" p ON p.id = s."planId"
			GROUP BY 1
			ORDER BY 1;
		`, [user.id]);

		const max = data.reduce((prev, current) => {
			return current.total > prev ? current.total : prev;
		}, 0);

		return {
			title: monthNames[(new Date()).getMonth()].toUpperCase(),
			minX: 0,
			maxX: data.length,
			minY: 0,
			maxY: max * 1.20,
			data,
		};
	}

	@Mutation(() => Sale, { name: 'saleCreate' })
	@IsPublic({ userRequired: false })
	async mutationSaleCreate(
		@Args('input') input: SaleCreateInput,
		@CurrentUser() user: User,
	) {
		const plan = await Plan.findOne(input.planId);
		if (!plan) throw new Error('Plano não encontrado!');

		return Sale.create({
			...input,
			plan,
			user: user ?? { id: "1" },
		}).save();
	}

	@Mutation(() => Sale, { name: 'saleUpdate' })
	@IsPublic()
	async mutationSaleUpdate(
		@Args() args: GetOneArgs,
		@Args('input') input: SaleUpdateInput,
		@Args({ name: 'admin', nullable: true, defaultValue: false }) admin: boolean,
		@CurrentUser() user: User,
	) {
		const sale = (admin && user.role === 'root') ?
			await Sale.findOne({ id: args.id }) :
			await Sale.findOne({ id: args.id, user });
		if (!sale) throw new Error('Registro não encontrado!');

		let data = {
			...input,
			confirmed: undefined,
			isConfirmed: undefined,
		};

		if (user.role === 'root' && input.isConfirmed !== null) {
			if (input.isConfirmed && sale.confirmed === null) {
				data.confirmed = new Date();
			} else if (!input.isConfirmed && sale.confirmed !== null) {
				data.confirmed = null;
			}
		}

		await Sale.update({ id: sale.id }, Object.fromEntries(Object.entries(data).filter(([key, value]) => value !== undefined && sale[key] != value)));
		await sale.reload();

		return sale;
	}
}
