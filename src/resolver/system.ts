import { Query, Resolver } from '@nestjs/graphql';
import { System } from '../model/system';
import Config from '../common/config';
import { IsPublic } from '../common/auth';

@Resolver()
export class SystemResolver {
	@Query(() => System, { name: 'system' })
	@IsPublic({ userRequired: false })
	querySystem() {
		return {
			name: Config.name,
			description: Config.description ?? '',
			currentTime: new Date(),
		};
	}
}
