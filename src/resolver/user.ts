import {
	Args,
	Mutation,
	Query,
	Resolver,
	Context,
	ResolveField,
	Root,
} from '@nestjs/graphql';
import { User } from '../model/user';
import { Person } from '../model/person';
import { UserConnection, AuthPayload, UserCreateInput, UserUpdateInput } from '../type/user.dto';
import { IsPublic, CurrentUser } from '../common/auth';
import { MyRequest } from '../common/request';
import { GetOneArgs, GetManyArgs } from '../model/base';
import { Session } from '../model/session';
import { Bcrypt } from '../common/util';
import configs from '../common/config';
import Messages from '../common/messages';
import debugModule from '../common/debug';
import { PersonInput } from '../type/person.dto';

const debug = debugModule('UserResolver');

@Resolver(() => User)
export class UserResolver {
	@ResolveField()
	@IsPublic()
	sessions(@Root() user: User) {
		return Session.ofUser(user);
	}

	@Query(() => User, { name: 'me' })
	@IsPublic()
	queryMe(@CurrentUser() user: User) {
		return user;
	}

	@Query(() => User, { name: 'user' })
	queryUser(@Args() args: GetOneArgs) {
		return User.findOneByArgs(args);
	}

	@Query(() => UserConnection, { name: 'users' })
	queryUsers(@Args() args: GetManyArgs) {
		return User.findPage(args);
	}

	@Mutation(() => AuthPayload, { name: 'login' })
	@IsPublic({ userRequired: false })
	async mutationLogin(
		@Args('email') email: string,
		@Args('password') password: string,
		@Context('req') req: MyRequest,
	) {
		const user = await User.findToLogin(email.trim().toLowerCase());
		if (!user) throw new Error(Messages.loginError);

		const passwordValid = await Bcrypt.compare(password, user.password);
		if (!passwordValid) throw new Error(Messages.loginError);

		const session = await Session.toUser(user, req.ip);
		await session.save();

		return {
			token: session.token,
			user,
		};
	}

	@Mutation(() => User, { name: 'userChange' })
	@IsPublic()
	async mutationUserChange(
		@Args('email', { nullable: true }) email: string,
		@Args('input', { nullable: true }) input: PersonInput,
		@CurrentUser() user: User,
	) {
		let data: { email?: string, person?: PersonInput } = {};
		if (email || input) {
			if (email) {
				data.email = email;
			}
			if (input) {
				data.person = input;
			}
		} else {
			return user;
		}

		await User.update({ id: user.id }, data).catch((err) => {
			debug('mutationUserChange', err);
			if (
				err.routine === '_bt_check_unique' &&
				err.constraint === 'UQ_78a916df40e02a9deb1c4b75edb'
			)
				throw new Error('E-mail já cadastrado!');
			throw new Error('Erro atualizando as informações, por favor tente novamente em alguns instantes');
		});

		await user.reload();
		return user;
	}

	@Mutation(() => Boolean, { name: 'userChangeOldPassword' })
	@IsPublic()
	async mutationUserChangeOldPassword(
		@CurrentUser() user: User,
		@Args('oldPassword') oldPassword: string,
		@Args('newPassword') newPassword: string,
	) {
		const passwordValid = await Bcrypt.compare(oldPassword, user.password);
		if (!passwordValid) throw new Error(Messages.wrongPassword);

		user.password = await Bcrypt.hash(newPassword, 10);
		await user.save();

		return true;
	}

	@Mutation(() => User, { name: 'userCreate' })
	async mutationUserCreate(
		@Args('input') input: UserCreateInput,
	) {
		if (input.password) {
			input.password = await Bcrypt.hash(input.password, 10);
		}
		return User.create(input).save();
	}

	@Mutation(() => User, { name: 'userUpdate' })
	async mutationUserUpdate(
		@Args() args: GetOneArgs,
		@Args('input') input: UserUpdateInput,
	) {
		const record = await User.findOne({ id: args.id });
		if (!record) throw new Error('Registro não encontrado!');

		if (input.password) {
			input.password = await Bcrypt.hash(input.password, 10);
		}
		await User.update({ id: record.id }, input);
		await record.reload();

		return record;
	}

	@Mutation(() => User, {name: 'userRegister'})
    async mutationUserRegister(
        @Args('email') email: string,
        @Args('password') password: string,
        @Args('role') role: string,
        @Args('input', { nullable: true }) input: PersonInput,
    ) {
        //cadastro pedente de aprovaçao
        let data: { email: string, password: string, role: string, person?: PersonInput } = {email, password, role};
        if (password) {
            data.password = await Bcrypt.hash(password, 10);
        }
        if (input) {
            data.person = input;
        }
        return User.create(data).save();
    }
}
