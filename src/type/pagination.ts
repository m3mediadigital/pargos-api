import { InputType, Field, ID, Int, ObjectType } from '@nestjs/graphql';

@InputType()
export class Pagination {
	@Field(() => Int, { nullable: true, defaultValue: 0 })
	page: number;

	@Field(() => ID, { nullable: true })
	after?: string;

	@Field(() => Int, { nullable: true, defaultValue: 20 })
	limit: number;

	@Field(() => Boolean, { nullable: true, defaultValue: false })
	disabled: boolean;
}

@ObjectType()
export class PageInfo {
	@Field(() => Int)
	count: number;

	@Field(() => Int)
	pages: number;

	@Field(() => Int)
	current: number;
}
