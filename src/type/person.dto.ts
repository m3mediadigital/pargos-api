import { Field, InputType, PartialType } from '@nestjs/graphql';
import { Address } from '../model/address';

@InputType()
export class AddressInput extends PartialType(Address, InputType) {}

@InputType()
export class PersonInput {
	@Field({ nullable: true })
	name?: string;

	@Field({ nullable: true })
	sex?: string;

	@Field({ nullable: true })
	rg?: string;

	@Field({ nullable: true })
	rgInfo?: string;

	@Field({ nullable: true })
	cpf?: string;

	@Field({ nullable: true })
	nationality?: string;

	@Field({ nullable: true })
	birthday?: Date;

	@Field({ nullable: true })
	maritalStatus?: string;

	@Field({ nullable: true })
	profission?: string;

	@Field({ nullable: true })
	phone?: string;

	@Field(() => AddressInput, { nullable: true })
	address?: AddressInput;
}
