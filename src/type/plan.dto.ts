import { Field, InputType, ObjectType, PartialType } from '@nestjs/graphql';
import { Connection } from '../model/base';
import { Plan } from '../model/plan';

@ObjectType({ implements: Connection })
export class PlanConnection extends Connection {
	@Field(() => [Plan])
	nodes: Plan[];
}

@InputType()
export class PlanCreateInput {
	@Field()
	name: string;

	@Field()
	value: number;

	@Field()
	comission: number;

	@Field({ nullable: true })
	info?: string;
}

@InputType()
export class PlanUpdateInput extends PartialType(PlanCreateInput) {}
