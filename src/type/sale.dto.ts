import { Field, ID, InputType, ObjectType } from '@nestjs/graphql';
import { Connection } from '../model/base';
import { Sale } from '../model/sale';
import { PageInfo } from './pagination';
import { AddressInput, PersonInput } from './person.dto';

@ObjectType({ implements: Connection })
export class SaleConnection extends Connection {
	@Field(() => [Sale])
	nodes: Sale[];
}

@ObjectType()
export class SaleComission {
	@Field(() => Sale)
	sale: Sale;

	@Field()
	comission: number;

	@Field()
	date: Date;
}

@ObjectType()
export class SaleComissionConnection {
	@Field(() => [SaleComission])
	nodes: SaleComission[];

	@Field(() => PageInfo)
	pageInfo: PageInfo;
}

@ObjectType()
export class SaleComissionGraph {
	@Field()
	title: string;

	@Field()
	minX: number;

	@Field()
	maxX: number;

	@Field()
	minY: number;

	@Field()
	maxY: number;

	@Field(() => [SaleComissionDay])
	data: SaleComissionDay[];
}

@ObjectType()
export class SaleComissionDay {
	@Field()
	day: Date;

	@Field()
	total: number;

	@Field(() => [ID])
	sales: string[];
}

@InputType()
export class SaleCreateInput {
	@Field(() => ID)
	planId: string;

	@Field(() => PersonInput, { nullable: true })
	person?: PersonInput;

	@Field({ nullable: true })
	email?: string;

	@Field(() => AddressInput, { nullable: true })
	address2?: AddressInput;

	@Field({ nullable: true })
	phone2?: string;

	@Field(() => [String], { nullable: true })
	dependents?: string[];
}

@InputType()
export class SaleUpdateInput {
	@Field({ nullable: true })
	isConfirmed?: boolean;

	@Field(() => PersonInput, { nullable: true })
	person?: PersonInput;

	@Field({ nullable: true })
	email?: string;

	@Field(() => AddressInput, { nullable: true })
	address2?: AddressInput;

	@Field({ nullable: true })
	phone2?: string;

	@Field(() => [String], { nullable: true })
	dependents?: string[];
}
