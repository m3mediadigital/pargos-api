import { Field, ObjectType, InputType, PartialType } from '@nestjs/graphql';
import { Connection } from '../model/base';
import { User } from '../model/user';
import { PersonInput } from './person.dto';

@ObjectType({ implements: Connection })
export class UserConnection extends Connection {
	@Field(() => [User])
	nodes: User[];
}

@ObjectType()
export class AuthPayload {
	@Field()
	token: string;

	@Field()
	user: User;
}

@InputType()
export class UserCreateInput {
	@Field()
	email: string;

	@Field()
	password: string;

	@Field()
	role: string;

	@Field(() => PersonInput)
	person: PersonInput;
}

@InputType()
export class UserUpdateInput extends PartialType(UserCreateInput) { }
